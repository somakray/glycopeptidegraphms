"""Uses toy data to show a programmer stepping through line by line how it works

the main function is run_toy_data()
This will execute through each of the steps of the basic functionality of The Pipeline.

Please use the example files in the directory: ../examples/02_pipeline_toy_inputs

"""

import os
from glob import glob

# sets the current working directory for stability of filepaths
dirpath = os.path.dirname(os.path.realpath(__file__))
print('setting directory to {}'.format(dirpath))
os.chdir(dirpath)

from graphms import pipeline

def run_toy_data():
    p = pipeline.datapipeline(top_folder='../examples/02_pipeline_toy_inputs/output',
                              overwrite=True,
                              consensusxml='../examples/02_pipeline_toy_inputs/Example_Data_AXL_truncated.csv',
                              byonic_psms='../examples/02_pipeline_toy_inputs/Byonic PSMs.csv',
                              graphms_files=tuple(glob('../examples/02_pipeline_toy_inputs/*.graphml')),
                              predcomp_file='../examples/02_pipeline_toy_inputs/pc.csv',
                              reference_file='../examples/02_pipeline_toy_inputs/pc_reference_nodes_out.xlsx',
                              )
    return



if __name__ == '__main__':
    run_toy_data()