#from bokeh.io import show, output_file
#from bokeh.charts import color, marker
from bokeh.plotting import figure, show, output_file
#from bokeh.layouts import layout
from bokeh.models import ColumnDataSource
import pandas as pd
import numpy as np
from graphms import custom_tools
from graphms import glycanformat as gf
import os
import shutil
import copy
import networkx as nx
import xlsxwriter

def graphquant(quant_file='', **kw):
    """converts an excel file containing glycan x peptide quant coordinates for a bar chart into a bokeh chart

    :param quant_file: (``string``) path to the PepQuant excel file to use as the input. Defaults to prompting user for input using a FileDialogBox.
    :param kw: see description.
    :return:

    ``Keyword Arguments``
        *  `threshold_percent`: (``float``) the value of the minumum relative abundance threshold to apply (only show those above this threshold)
        * `sort_by_neuac`: (``bool``) whether to sort the glycans into sialylated and unsialylated species; useful for comparisons.
        * `sort_by_antenna`: (``bool``) whether to sort the glycans into bi, tri, tetra antennary, for comparison purposes.
        * `save_filename`: (``string``) the path of the html file to save the bokeh quant graph as.
        * `scaled_values_save_filename`: (``string``) the path of the excel file containing the scaled values used to construct the plot, for traceability.

    """

    threshold = kw.get('threshold_percent', 0.) # if kw.get('threshold_percent')!=None else 0.
    try:
        threshold = float(threshold)
    except:
        print('Error. Threshold specified could not be converted to a number')
        return
    ### opens the xlsx file containing the label-free quant of gpeps
    filein = custom_tools.getfile('Select input gpep quantitation file (i.e. PepQuant)','.xlsx') if len(quant_file) < 1 else quant_file
    if len(filein)<1:
        print('Error. No input file was specified')
        return


    df = pd.read_excel(filein, sheetname=0, header=0,
                       index_col=None, na_filter=True, 
                       na_values='0',) 
    

    
    # force the pepgroup to string
    df.loc[:, 'pepgroup'] = df.loc[:, 'pepgroup'].apply(str)
    
    # first, get the y values by making a superset of all compositions
    comp_dup = df['composition'].duplicated() | df['composition'].isnull()
    y = df[['composition']][~comp_dup]
    
    ### if I want to do any sorting, I should do it here:
    # DO CUSTOM SORT by mass:
        
    ascending_sort=True # set to false to sort small glycans at top of page
    
    y['mass'] = y['composition'].apply(gf.glycancomptomass)
    y = y.sort_values('mass', axis=0, ascending=ascending_sort)
    
    
    if kw.get('sort_by_neuac', False)==True:
        neuac_mask = y['composition'].apply(lambda x: True if 'S' in x else False)
        y = pd.concat([y.loc[~neuac_mask, :],y.loc[neuac_mask,:]], axis=0, ignore_index=True)
    
    if kw.get('sort_by_antenna', False)==True:

        def count_antenna(comp):
            import glycanformat
            fourcomp = glycanformat.glycan_get_comp(comp)
            
            if ((fourcomp['N']>= 4) and (fourcomp['H']>= 5)):
                return int(np.floor(((fourcomp['N']-2)+(fourcomp['H']-3))/2))     
            else:
                return 0
        
        
        y['number_of_antenna'] = y['composition'].apply(count_antenna)
        y = y.sort_values(by=['number_of_antenna','mass'], ascending=ascending_sort)

    
    ### separate the pepgroup into the different unique gpeps
    
    # NB: you can control the groups by setting the different
    # peptide truncated names. This code will simply find the
    # unique ones.
    # unique_peps must be a series so that the for loop works
    # (fails on dataframe)
    unique_peps = df['pepgroup'][~df[['pepgroup']].duplicated()]
    pep_list = list(df[['pepgroup']][~df[['pepgroup']].duplicated()].iloc[:,0])
    
    
    ### create a new dataFrame and split the pepgroup there,
    # using the glycan list as index
    df_peps = pd.DataFrame(index=y['composition'], columns=None)
    df_norm = pd.DataFrame(index=y['composition'], columns=None)


    intscale = []
    
    for i, pep in enumerate(unique_peps):

        df_slice = df.loc[df['pepgroup']==pep, ['composition', 'pepgroup', 'sum_intens_similar_values']]

        df_slice.rename(columns={'sum_intens_similar_values' : pep}, inplace=True)
    
        #for the case where there is a duplicate, i can't concat
        # because the indices will overlap!
        # so, first combine duplicates in mz_slice!
        grouped = df_slice.groupby(['composition', 'pepgroup']).sum()
        grouped.reset_index(drop=False, inplace=True)  
        grouped.set_index('composition', drop=True, inplace=True)
        df_slice = grouped            
    
        intscale_current = 0.8/max(grouped[pep])
        intscale.append(intscale_current)
        
    
        # now concat the slice onto the df_peps
        df_peps = pd.concat([df_peps, df_slice[[pep]]], axis=1, join_axes=[df_peps.index])
    
        # and reorder the indices by the y axis (which is sorted by mass) (just in case)
        df_peps = df_peps.loc[y['composition'],:]
        
        # create a normalised intensity version (normalised to each peptide)
        norm = df_slice.loc[:, [pep]] / max(df_slice.loc[:, pep])
        df_norm = pd.concat([df_norm, norm], axis=1, join_axes=[df_norm.index])
    
    
    # finally, create an overall value!
    sum_list = []
    for index, x in df_peps.iterrows():
        sum_list.append(x.sum())
    
    # and add the TOTAL value to the dataframe
    df_peps['total']=sum_list
    
    
    # normalise the TOTAL column and then filter by threshold
    df_norm = pd.concat([df_norm, df_peps['total']/max(df_peps['total'])], axis=1, join_axes=[df_norm.index])    
    df_normcut = df_norm.loc[df_norm['total'] >= threshold,:]
    df_normcut.fillna(value=0.0, inplace=True)
    # define a dataframe for the columndatasource, with scaled values    
    df_scaled = pd.DataFrame(index=df_normcut.index)
    for i, col in enumerate(df_normcut.columns):
        df_scaled.loc[:,col] = df_normcut.loc[:,col] * 0.8 + i+0.5        
        df_scaled.loc[:,col+'left'] = i+0.5
    
    # need to adjust by +0.5 for the y-axis    
    df_scaled.loc[:, 'y'] = [x+0.5 for x in range(0, len(df_scaled),1)]
    
    
    source = ColumnDataSource(df_scaled)
    


    # define the number of glycans
    num_gly = len(df_scaled.index)
    
    # set the plot height as a function of number of values
    height = num_gly * 15
    
    # draw the figure
    p = figure(title= "Label-free Quantitation",
               x_axis_label='Peptide Backbone',
               x_range= list(df_scaled[pep_list+['total']].columns) + [''],
               y_axis_label='Glycoform',
               y_range=list(df_scaled.index),               
               plot_width=900,
               plot_height=height,
               sizing_mode="scale_width",
               ) # extra blank categories are space to the right so i can fit my overlaid labels    
    p.yaxis.axis_label_standoff = 30
    p.yaxis.axis_label_text_color = "#000000"
    
    p.output_backend = kw.get('bokeh_output_backend', 'canvas')
               
    p.xaxis.axis_label_text_font_size = "18pt"
    p.xaxis.major_label_text_font_size = '12pt'
    
    p.yaxis.axis_label_text_font_size = "18pt"
    p.yaxis.major_label_text_font_size = '12pt'

    # writer = pd.ExcelWriter(custom_tools.getsavefile('Name the output excel file containing all the scaled values', '.xlsx'))
    # writer = pd.ExcelWriter(os.path.splitext(filesave_html)[0] + '_pepquant.xlsx')
    # writer = pd.ExcelWriter(kw.get('scaled_values_save_filename','') or os.path.splitext(filesave_html)[0] + '_pepquant.xlsx')
    # df_peps.to_excel(writer, 'raw values')
    # df_normcut.to_excel(writer, 'scaled values (norm max)')
    # writer.save()
    
    for col in (df_scaled[pep_list+['total']].columns):
        print('column=',col)
        p.hbar(source=source,
               y='y',
               height=0.1,
               right=col,
               left=str(col+'left')
               )

    # custom_tools.getsavefile('Name the file to save the bokeh plot as html file', '.html')
    filesave =  kw.get('save_filename', '') or custom_tools.getsavefile('Name the output html file', '.html')
    if os.path.splitext(filesave)[1] != 'html':
        filesave = os.path.join(os.path.splitext(filesave)[0] + '.html')
    output_file(filename=filesave)
    
    show(p)    

    # output the scaled values in an excel file
    writer = pd.ExcelWriter(kw.get('scaled_values_save_filename','') or os.path.splitext(filesave)[0]+'_scaledvalues.xlsx')
    (df_peps.fillna(value=0)).to_excel(writer, 'grouped raw values')
    (df_norm.fillna(value=0)).to_excel(writer, 'normalised values')
    df_normcut.to_excel(writer, 'normalised & ' + str(np.round(threshold,2)) + 'threshold')
    df_scaled.to_excel(writer, 'scaled values for plotting')    
    
    writer.save()
    
    custom_tools.opendir(os.path.dirname(filesave))
    
    return dict(htmlpath=filesave,
                excelpath=os.path.splitext(filesave)[0]+'.xlsx',
                )

def quantgraphtonodes(abscomp_reference_file='', consensus_list=[], rt_corrections=[], mz_tol=0.02, rt_tol=200):
    """
    this takes an absolute composition file (final output from GraphMS), which you need to have filtered for the
    subgraphs of interest.
    Takes in a LIST of consensus csv
    Output - list of abscomp glycans but tagged with the correspnding nodes in the consensusxml csv files with their quants.
    :param abscomp_reference_file:
    :param consensus_list:
    :param rt_corrections:
    :param mz_tol:
    :param rt_tol:
    :return:
    """
    abscomp_reference_file = custom_tools.getfile('Select the reference abscomp file for quant vs nodes.',
                                                  'xls*') if len(abscomp_reference_file)==0 else \
                                                  abscomp_reference_file
    consensus_list = custom_tools.getfiles('Select the list of consensusxml files to do quant on',
                                           '.csv') if len(consensus_list)==0 else \
                                           consensus_list

    # if no RT corrections were specified, simply make a 0 array of required length.
    rt_corrections = [0]*len(consensus_list) if len(rt_corrections)==0 else rt_corrections
    
    df_abs = pd.read_excel(abscomp_reference_file, index_col=None, header=0)
    

    original_columns = list(df_abs.columns)
    savefile = custom_tools.getsavefile('please name the output node matched file.','.xlsx')
    writer = pd.ExcelWriter(savefile)

    try:
        shutil.copy(abscomp_reference_file, os.path.dirname(savefile))
        [shutil.copy(c, os.path.dirname(savefile)) for c in consensus_list]
    except:
        pass


#    df_abs_new=[]
    for index, (file, rtcorrection) in enumerate(zip(consensus_list, rt_corrections)):
        
        
        print('running file: ', file)
        df_abs = matchmzrt(reftable=df_abs, #.loc[:,['mz_node', 'rt_node', 'intensity', 'composition', 'pep']],
                                 targettable=pd.read_csv(file, index_col=None, header=0).loc[:, ['mz_cf','rt_cf', 'intensity_cf']],
                                 rt_correction = rtcorrection,
                                 mz_tol=mz_tol, 
                                 rt_tol=rt_tol,
                                 targetname=str(index)
                                 )
#        df_abs_new.append(df_matched)
    
#    df_final = pd.concat([df for df in df_abs_new], axis=1)
        
    

    print('doing final processing')

    final_columns = original_columns + [col for col in df_abs.columns if col not in original_columns ]
    df_abs = df_abs[final_columns]
#    pdb.set_trace()

    # get the name of the columns that derive from the consensus files
    matchcols = df_abs.filter(regex=('mz_\d+'))
    # remove lines where there are no matches and intensity is zero.
    df_abs = df_abs.loc[((matchcols > 0).sum(axis=1) > 0) | (df_abs['intensity']>0), :]

    # if all blank, remove

    # if some triplicates are all zeroes
        
    
    df_abs.to_excel(writer, 'nodematched')
    # save the name of the files used
    pd.DataFrame([(y,x) for y in [abscomp_reference_file] for x in consensus_list], columns=['reference file', 'consensus file that was matched to reference file']).to_excel(writer, 'metadata')
    # save the RT corrections
    pd.DataFrame([x for x in rt_corrections], columns=['RT corrections made']).to_excel(writer, 'RT corrections')

    # save the input files also
    pd.read_excel(abscomp_reference_file, index_col=None, header=0).to_excel(writer, 'input_abscomp_file')
    for index, f in enumerate(consensus_list):
        pd.read_csv(f, index_col=None, header=0).to_excel(writer, 'input_consensus_' + str(index))

    writer.save()

    return savefile


    

def matchmzrt(reftable, targettable, rt_correction, mz_tol, rt_tol, targetname='target'):
    
    #apply RT correction factor to the targettable
    # corr = lambda x : x - (0.0000214*(x**2) - 0.023743*x+3.0524)
    # targettable['rt_cf'] = targettable['rt_cf'].apply(corr)
    
    print('processing matchmzrt...')
    subtraction_mz = np.array(reftable.loc[(reftable['intensity']>0),['mz_node']]) - np.array(targettable[['mz_cf']]).T
    subtraction_rt = np.array(reftable.loc[(reftable['intensity']>0),['rt_node']]) - np.array((targettable[['rt_cf']]-rt_correction)).T
    
    # this gets a list of tuple pairs of matches
    pairs = list(zip(*np.where((abs(subtraction_mz) <= mz_tol) & (abs(subtraction_rt) <= rt_tol))))
#    pdb.set_trace()
    
    
    G = nx.DiGraph()
    G.add_edges_from(pairs) 
    
    original_columns = ['node', 'mz_node', 'rt_node', 'intensity', 
                        'refnode', 'mz_refnode', 'mzdifference', 
                        'sum_n', 'sum_h', 'sum_f', 'sum_s', 'N', 
                        'H', 'F', 'S', 'composition', 'pep', 
                        'subgraph', 'pepgroup']
    
    
    for counter in range(2):
        graph_edges = G.edges
        node_degrees = G.degree()
        nodes_with_multiple_connections = [x for x in node_degrees if node_degrees[x]>1]
        nodes_one_to_many = [x for x in nodes_with_multiple_connections if len(graph_edges(x))>1]
        nodes_many_to_one = [x for x in nodes_with_multiple_connections if len(graph_edges(x))==0]
#        pdb.set_trace()
        nodes_iter = copy.copy(G.nodes_iter())
    #    print('G.degree() before trimming = \n', G.degree())
        for node in nodes_iter:
            
            if node in nodes_one_to_many:
                # sum up intensities
                # create a dict with empty values, then if later on the node is in this dict
                # can simply lookup the summed intensity value here
                
                # that, or append extra lines to the reftable dataframe, to match
                # to the peaks in targettable. And alter the edges accordingly,
                # to point to those extra lines. 
                # ^This is better because I don't compress the peaks. I can then delete them manually if I need.
                # with the above strategy, I would lose the ability to discriminate!
                
                ## add new cloned lines to reftable
#                ref_slice = reftable.iloc[[node],:].loc[:,original_columns]
                ref_slice = reftable.iloc[[node],:]
                for col in list(ref_slice.columns):
                    if col not in original_columns:
                        ref_slice[col]=np.nan
                    
                
#                print('ref slice = ', ref_slice)
                ref_slice.loc[:,'intensity']=0
                # note that len will give me the +1 already because the index starts at zero.              
    #            print('\ntotal edges: {} \nlist of edges to process:{}'.format(graph_edges(node), graph_edges(node)[1:]))
                for (origin, target) in graph_edges(node)[1:]:
                    # pdb.set_trace()
#                    if len(reftable.index)==493: pdb.set_trace()
                    current_max_index_reftable = len(reftable.index)    
    #                print('max len =', current_max_index_reftable)
                    # assume that this node must be in reftable!            
                    ref_slice.index=[current_max_index_reftable]
#                    reftable = reftable.append(ref_slice)                    
                    reftable = pd.concat([reftable, ref_slice],axis=0, join='outer')
                    # add in the new edge to the new row
                    G.add_edge(current_max_index_reftable, target)
                    # delete the old edge
                    try:
                        G.remove_edge(origin, target)
    #                    print('removed edge at = ', (origin, target,))
                    except:
                        print('removing node failed')
    #                print('new index, origin, target = ', current_max_index_reftable, origin,target)
    #                print('degrees = \n{}'.format(G.degree()))
                    
                # make edges point to here instead
            
            if node in nodes_many_to_one:
                # delete all but first edge
    #            pdb.set_trace()
    #            print('many to one iterator =', [edge for index, edge in 
    #                     enumerate(G.in_edges_iter(node)) 
    #                    if index>0])
                G.remove_edges_from(
                        [edge for index, edge in 
                         enumerate(G.in_edges_iter(node)) 
                        if index>0])    

    ## do a final check

        
#    print('G.degree() after trimming = \n', G.degree())
    node_degrees = G.degree()
    new_nodes_with_multiple_connections = [x for x in node_degrees if node_degrees[x]>1]
    print('after making the Graph one-to-one, the resulting nodes with more than one connection are =\n',
          new_nodes_with_multiple_connections)
#    pdb.set_trace()            
    
#    for node in G.nodes_iter():
#        for (starter, target) in G.edges([node]):
#            intensity_tally += targettable.loc[G.edges(target)[1], 'intensity_cf']
#        intensity_tally
    
    # better to do line by line matching so I can control the 

   
    
    mzcol = ''.join(['mz_', targetname])
    rtcol = ''.join(['rt_', targetname])
    intcol = ''.join(['col_', targetname])
    
    targettable.rename(columns=dict(zip(['mz_cf', 'rt_cf', 'intensity_cf'],[mzcol, rtcol, intcol])), inplace=True)
    
#    pdb.set_trace()
    for col in (mzcol, rtcol, intcol): reftable.insert(len(reftable.columns), col, np.nan)     
    
    # here, need to remake the pairs, extracting from edges instead!
    pairs = G.edges()
    for (origin, target) in pairs:
#        reftable.loc[[pair[0]], [mzcol, rtcol, intcol]] = \
#            targettable.loc[pair[1], ['mz_cf', 'rt_cf', 'intensity_cf']]            
            
#        reftable=pd.concat([reftable,targettable.loc[[pair[0]]]], axis=1)
        
        df_slice = targettable.loc[[target]]
        df_slice.index=[origin]
        
        reftable.update(df_slice)
    
        
        
    
#    pdb.set_trace()    
        
    return reftable


def extract_and_combine_node_to_node_outputs(filelist_in=[]):

    # force the type for the input file; put it in a list if needed.
    if not isinstance(filelist_in, list):
        if isinstance(filelist_in, str):
            filelist_in = [filelist_in]
        else:
            filelist_in = []
    else:
        filelist_in = []

    filelist = filelist_in or custom_tools.getfiles('select the node-to-node output files to combine and extract', '.xlsx')

    df_result = pd.DataFrame()

    for file in filelist:
        df_file = pd.read_excel(file, sheetname='nodematched', header=0, index_col=0)
        # sum together the matching peptide glycoform peaks and extract the intensities
        # df_grouped = df_file.groupby(['pep', 'composition']).sum().loc[:, ['col_0', 'col_1', 'col_2']]
        df_grouped = df_file.groupby(['pep', 'composition']).sum().filter(regex='col_\d+')
        df_grouped.columns = [str(x) + '_' + os.path.splitext(os.path.basename(file))[0] for x in range(1,len(df_grouped.columns)+1)]
        # df_grouped.columns = ['1_' + os.path.basename(file),
        #                       '2_' + os.path.basename(file),
        #                       '3_' + os.path.basename(file)]

        df_result = pd.concat([df_result, df_grouped], axis=1, ignore_index=False)

    # slice away those rows with absolutely no values? no need.

    # then set the triplicates with all 0 to a value of 0.
    number_of_replicates = 3
    for i in range(0,len(df_result.columns),number_of_replicates):
        # this sets the whole row to a single value, zero.
        df_result.loc[df_result.iloc[:, i:i+3].sum(axis=1)==0, i:i+3] = 0


    outfile = custom_tools.getsavefile('please name the output file of the extracted values', '.xlsx')

    writer = pd.ExcelWriter(outfile)
    df_result.to_excel(writer, sheet_name='result', engine='xlsxwriter')
    writer.save()
    print("Extract and combine finished.")
    return outfile, df_result


def do_node_to_node():
    # probably should make this an input box instead! use simple one from custom tools
    cons_file_list = custom_tools.getfiles('select consensus csv files to do the node to node quants', '.csv')
    abscomp_file = custom_tools.getfile('select file containing the reference as abscomp', '.xlsx')

    quant_save_file = quantgraphtonodes(abscomp_reference_file=abscomp_file,
                     consensus_list=cons_file_list,
                     rt_corrections=[],
                     rt_tol=60,
                     )

    extract_and_combine_node_to_node_outputs(quant_save_file)

    savefolder = os.path.dirname(quant_save_file)

    for f in cons_file_list + abscomp_file:
        try:
            shutil.copy(f, os.path.join(savefolder, os.path.basename(f)))
        except:
            print(f, '...file already exists. not copying file.')

    custom_tools.opendir(savefolder)
    

if __name__ == '__main__':
    # graphquant('', threshold_percent=0.05, sort_by_neuac=False,
    #            sort_by_antenna=False,
    #            bokeh_output_backend='svg')
    do_node_to_node()
