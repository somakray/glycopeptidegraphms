# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 11:12:14 2017

@author: choom

this is mean to store all the bokeh plotting that needs to happen.
by centralising the plotting, I get control and efficiency of
code. Making it easier to tweak.
"""
# from bokeh.io import show, output_file
from bokeh.plotting import figure, show, output_file
from bokeh.models import HoverTool, WheelZoomTool, BoxZoomTool, PanTool, ResetTool, \
    SaveTool, LassoSelectTool, UndoTool
from bokeh.models import ColumnDataSource, Legend
from bokeh.models.glyphs import MultiLine
from bokeh.models import Range1d
# for drawing multiple axes
from bokeh.models import LinearAxis, FuncTickFormatter, DataRange1d, Range1d

import pandas as pd
import numpy as np
import networkx as nx

from graphms import custom_tools
from graphms import glymod_functions
from graphms import writenodeid

import pdb
import os
import sys
from random import randint

from collections import OrderedDict

from matplotlib import pyplot as plt

cnames = custom_tools.cnames()



class subgraphfigure:


    def __init__(self, has_alignment_data=False):
        self.wheel_zoom = WheelZoomTool()
        self.has_alignment_data = has_alignment_data

        self.HOVER_TUPLE = [("subgraph", "@subgraph"),
                       ("m/z", "@mz{1.11111}"),
                       ("RT", "@rt{1.1111} s; @rt_minutes{1.11} m"),
                       ("Precursor(mz+z@rt)", "m/z @precursor_mz{1.1111}(+@precursor_charge) @ @precursor_rt{1.1111} s"),
                       ("Composition", "HexNAc@N Hex@H Fuc@F NeuAc@S"),
                            ("Peptide", "@abscomp_gpep"),
                            ('Protein', '@protein'),
                        ("deltaMZ", "m/z @delta_mz"),
                        ("deltaRT", "@delta_RT s"),
                       ]

        if self.has_alignment_data:
            # self.HOVER_TUPLE += [("Peptide", "@abscomp_gpep"), ('Protein','@protein')]
            pass

        self.hover = HoverTool(tooltips=self.HOVER_TUPLE)

        self.p = figure(plot_width=1000, plot_height=300,
                        sizing_mode="stretch_both",
                        tools=[self.wheel_zoom, self.hover, BoxZoomTool(),
                               PanTool(), UndoTool(), ResetTool(),
                               SaveTool(), LassoSelectTool()]
                        )
        self.p.toolbar.active_scroll = self.wheel_zoom

        self.col_graph = parsecolors(['red',
                                      'blue', 'greenyellow',
                                      'cyan', 'magenta',
                                      'black', 'slateblue',
                                      'seagreen', 'tan',
                                      'gray', 'yellow',
                                      ])
        self.graphmls = []

        return


    def plot(self):
        return self.p

    def add_subgraphs_to_plot(self, graphmls=[], graphoptions={}, filename_filter_to_exclude=None):
        """Adds graphs encoded in a list of graphml files  to the plot.

        :param graphmls:
        :param graphoptions:
        :param filename_filter_to_exclude:
        :return:
        """
        print('adding graphs to plot...')

        # prompt for graphml files if no files were provided.
        graphmls = list(custom_tools.getfiles('choose graphml files, or cancel to stop the plotting', '.graphml')) if len(graphmls) == 0 else graphmls
        # exit if no graphml files are provided even after the prompt.
        if custom_tools.islistempty(graphmls):
            raise Exception('No files inputted to bokehgraphms.add_subgraphs_to_plot(). Exiting subroutine.')
            return

        self.graphmls = graphmls

        # if not defined, set the output html file's directory to where the graphmls were, and suffixed
        # with a large random number
        if (graphoptions.get('output_html_file_path', None) == None):
            graphoptions['output_html_file_path'] = \
                os.path.join(os.path.dirname(graphmls[-1]), ''.join(['outputgraph', str(randint(1, 100000)), '.html']))
        else:
            graphoptions.get('output_html_file_path', '')

        # get the graphoptions into a function-readable format
        graphoptions = parseoptions(graphoptions)  # access with graphoptions['export_csv']

        # initialise list of glyphs that will be created in order to populate the legend
        glyph_list = []
        glyphappend = glyph_list.append

        ### Update the hovertool, using the last graphml because that's probably the smallest
        G_for_hovertool_updating = nx.read_graphml(path=graphmls[-1], node_type=int)
        # modcol -> jumplist column names; modextract -> glycan names minus "glymod_";
        # abbrev -> the SNFG abbreviated equivalents
        modcol_list, modextract_list, abbrev_list = \
            writenodeid.get_glycan_abbrevs_from_single_graph(G_for_hovertool_updating)

        # need to be careful when updating the hovertool, because the jumplist's glymod
        # names may not always reflect the node properties.
        # as a check, print out the non-zeroth node properties
        index_of_first_of_the_nonzeroth_node = [x for x in G_for_hovertool_updating.nodes if str(x)!='0'][0]
        print('Node Properties are:\n', G_for_hovertool_updating.nodes[index_of_first_of_the_nonzeroth_node])
        print('Expected names are:\n', modextract_list)

        self.update_hovertool_with_glycan_list(abbrev_list,modextract_list, modcol_list)

        ### Get Stats on all the graphs, in order to calculate the min and max circle sizes relative to the data.

        # need to check all the files for global max and min
        # in order to scale correctly.
        int_list = np.array([])
        rt_list = np.array([])

        for fileitem in graphmls:
            # load into graph and get the array of intensities
            intensities, rts = get_intensity_and_rt_from_graphml(fileitem)
            int_list = np.append(int_list, intensities)
            rt_list = np.append(rt_list, rts)

        # now, get the stats on this total list.
        self.getintensitystats(int_list)
        rt_min, rt_max = np.min(rt_list), np.max(rt_list)

        ### add subgraph

        for index, fileitem in enumerate(graphmls):
            self.index = index  # this sets the index for the file being processed; need it for naming subgraphs

            # apply the negative filter, and skip if there's a match
            if filename_filter_to_exclude != None:
                if filename_filter_to_exclude in fileitem:
                    continue

            glyph_and_label_list_for_legend = self._add_subgraph_to_plot(fileitem, graphoptions)
            glyphappend(glyph_and_label_list_for_legend)

        # i could go back and retrospectively change all the intensity scaling.
        # using glyphappend, which should contain all the glyphs.

        ### make bokeh plot
        print('Plotting bokeh chart of data...')
        self.p.title.text = 'Subgraphs by color'

        ### make interactive legend

        # sort order of glyphs alphabetically (101019 turned this off, because it sorts digits incorrectly)
        # glyph_list.sort()

        legend = Legend(items=glyph_list, location='top_right', click_policy='hide')
        self.p.add_layout(legend, 'right')
        # p.legend.visible=False

        # add second minutes x axis
        self.p.extra_x_ranges = {'minutes': DataRange1d()}
        self.p.add_layout(LinearAxis(x_range_name='minutes'), 'below')
        # double slash // is floor division.
        self.p.xaxis[1].ticker = list(np.arange(rt_min // 60 * 60, (rt_max // 60 + 2) * 60, 15))
        self.p.xaxis[1].formatter = FuncTickFormatter(code="""
                    var labels = tick/60;
                    return labels.toString().concat(' min');
                    """)

        # format tick lines
        self.p.axis.major_tick_out = 10

        self.p.toolbar_location = 'above'
        self.p.background_fill_color = None
        self.p.border_fill_color = None
        self.p.output_backend = graphoptions['output_format']
        output_file(graphoptions['output_html_file_path'])
        show(self.p)

        return

    def _add_subgraph_to_plot(self, graph_file='', graphoptions={}):

        #        graphoptions = parseoptions(graphoptions) # access with graphoptions['export_csv']

        # since this is only for single files, if a list of files is passed,
        # simply take the first item.
        if isinstance(graph_file, list): graph_file = graph_file[0]

        if graphoptions['export_csv']:
            pass  # no longer using this option

        ### read Graph
        G = nx.read_graphml(path=graph_file, node_type=int)
        self.df_all_edges_with_origin_dest = self.formatgraph(G)
        self.df_only_glymods_and_color, self.glycan_mods_fullname_list, G = self.getzerothnode(G)

        ### plot edges
        edge_glyph_list = self.plotedges() if graphoptions['show_edges'] == True else []

        ### plot nodes
        node_glyph = self.plotnodes(G, graphoptions, graph_file)

        return (str(os.path.splitext(
            os.path.basename(graph_file))[0]),
                list(edge_glyph_list) + list(node_glyph),
                )

        # return the list of glyphs(circles+lines) and graphml file names

    #        return list(glyphlinelist) + list(edge_glyph_list), list(glyph_list)

    def plotnodes(self, G, graphoptions, f):
        # this now draws the nodes over the edges

        # here, we extract the desired data from the inputted Graph, G.
        # nb: only those data extracted here will display on the graph!
        list_of_keys_from_graph = G.node[list(G.nodes())[-1]].keys()
        data_dict = OrderedDict()

        for key in list_of_keys_from_graph:
            data_dict[key] = nx.get_node_attributes(G, key)

        # data_dict = OrderedDict(
        #     mz=nx.get_node_attributes(G, 'mz'),
        #     rt=nx.get_node_attributes(G, 'rt'),
        #     intensity=nx.get_node_attributes(G, 'intensity'),
        #     precursor_mz=nx.get_node_attributes(G, 'precursor_mz'),
        #     precursor_rt=nx.get_node_attributes(G, 'precursor_rt'),
        #     precursor_charge=nx.get_node_attributes(G, 'precursor_charge')
        #     )
        # if self.has_alignment_data:
        #     data_dict['abscomp_gpep'] = nx.get_node_attributes(G, 'abscomp_gpep')
        #     data_dict['aligned_gpep'] = nx.get_node_attributes(G, 'aligned_gpep')

        df = pd.DataFrame(data_dict,
                          columns=data_dict.keys())  # ['mz', 'rt', 'intensity', 'precursor_mz', 'precursor_rt', 'precursor_charge'])
        # df = pd.concat([df, pd.DataFrame([str(os.path.basename(f))] * nx.number_of_nodes(G))],axis=1,ignore_index=True)
        df['subgraph'] = [str(os.path.basename(f))] * nx.number_of_nodes(G)
        # subgraph = [str(os.path.basename(f))] * nx.number_of_nodes(G)

        # scale the intensity
        df['intensity_scaled'] = self.scale_intensity(df['intensity'], graphoptions['scaling']) * \
                                 (graphoptions['circle_size_max'] - graphoptions['circle_size_min']) + \
                                 graphoptions['circle_size_min']

        # convert to minutes
        df['rt_minutes'] = df['rt'] / 60

        # this eliminates the zeroth metadata node from the plot
        df = df.loc[df.loc[:, 'mz'] > 0.0, :]

        source = ColumnDataSource(df)
        circle_color = self.col_graph[(self.index % len(self.col_graph))]

        if graphoptions['show_edges']:
            glyph = self.p.circle('rt', 'mz', size='intensity_scaled',
                                  source=source, fill_alpha=0.2,
                                  fill_color=cnames['white'],
                                  line_color=circle_color, line_width=2.0,
                                  muted_color=circle_color, muted_alpha=0.1)


        else:
            # circles should look  different when I don't want to have the edges.
            if graphoptions['style'] == 'watercolor':

                glyph = self.p.circle('rt', 'mz', size='intensity_scaled',
                                      source=source, fill_alpha=0.4,
                                      fill_color=circle_color,
                                      line_color=circle_color,
                                      line_width=1.0,
                                      line_alpha=0.7,
                                      muted_color=circle_color, muted_alpha=0.1)

            else:
                glyph = self.p.circle('rt', 'mz', size='intensity_scaled',
                                      source=source, fill_alpha=1.0,
                                      fill_color=circle_color,
                                      line_color=cnames['black'],
                                      line_width=1.0,
                                      line_alpha=0.2,
                                      muted_color=circle_color, muted_alpha=0.1)

        return [glyph]

    def plotedges(self):
        edge_plot_list = []
        ## plot edges
        for row_index, current_jump_color in enumerate(self.df_only_glymods_and_color.loc[:, 'color']):
            # here, I just iterate over the color column, and use the row_index to slice the df's row into a dictionary
            current_jump_glymods_as_dict = self.df_only_glymods_and_color.iloc[row_index, :].to_dict()
            # now I can refer to this using my string keys!

            # this creates a blank mask as a starting point, must be np.array
            mask_boolean_all_match_and = (np.array([True] * \
                                                   len(self.df_all_edges_with_origin_dest)))

            ### all this must be done as numpy array not as dataframe in order to get the masking operation to work.

            # need to loop through and for each current_glymod_item,
            # find those edges that correspond to its composition.
            # To do this, create a boolean mask for the condition,
            # namely, where the column of the df of edges ==
            # the value of the current glymod item.
            # then merge the arrays with AND comparison.
            for current_glymod_item in self.glycan_mods_fullname_list:
                mask_boolean_matches_current_glymod = \
                    np.array(
                        self.df_all_edges_with_origin_dest.loc[:, current_glymod_item] == \
                        current_jump_glymods_as_dict[current_glymod_item]
                    )
                mask_boolean_all_match_and = (mask_boolean_all_match_and &
                                              mask_boolean_matches_current_glymod)

            # filter the edges to those only matching the current jump
            df_edges_matching_current_jump = self.df_all_edges_with_origin_dest.loc[mask_boolean_all_match_and, :]
            if len(df_edges_matching_current_jump) == 0: continue

            #            print(df_edges_matching_current_jump.iloc[[0],:])

            # glyphline = p.add_glyph(cds, MultiLine(xs='xs', ys='ys', line_color='black', line_width=2))
            # construct a columndatasource encapsulating these edges
            cds_from_edges = ColumnDataSource(df_edges_matching_current_jump)
            edge_plot_list.append(self.p.add_glyph(cds_from_edges,
                                                   MultiLine(xs='xs',
                                                             ys='ys',
                                                             line_color=cnames[current_jump_color],
                                                             line_width=1.5,
                                                             line_alpha=0.8)
                                                   )
                                  )

        return edge_plot_list

    def getzerothnode(self, G):

        self.jumplist_all, self.jumplist_cluster, self.jumplist, islegacy = extract_metadata_from_zeroth_node(G)
        #        print('jumplist_all = \n', self.jumplist_all)

        if islegacy:
            glycan_mods_fullname_list = list('NHFS')
            # print('legacy edge grouping invoked.')
        else:
            ## get the glycan mods from the dfs
            # glycan_mods_fullname_list is what I want to use?
            glycan_mods_fullname_list, glycan_mods_extracted_list = \
                glymod_functions._extractglymod(df=self.jumplist_all, delimiter='glymod_')
            # print('normal edge grouping invoked.')
            removezerothnode(G)

        # this isolates only the glymod and color columns from the jumplist
        df_only_glymods_and_color = self.jumplist_all.loc[:, ['color'] + glycan_mods_fullname_list]

        return df_only_glymods_and_color, glycan_mods_fullname_list, G

    def getintensitystats(self, intlist):
        # sets the 25% threshold, above which lies 75% of the data.
        # this mitigates the effect of the small peaks

        intlist = np.array(intlist)
        intlist = intlist[intlist > 0]
        loglist = np.log10(intlist)
        self.percentile75 = np.percentile(loglist, q=25, interpolation='linear')

        # set the 15% and 85% marks
        self.percentile15 = np.percentile(loglist, q=15, interpolation='linear')
        self.percentile85 = np.percentile(loglist, q=85, interpolation='linear')

        # get the min and max
        self.percentile0 = np.min(loglist)
        self.percentile100 = np.max(loglist)
        self.logrange = self.percentile100 - self.percentile0

        # linear scaling min and max
        self.linearmin = np.min(intlist)
        self.linearmax = np.max(intlist)
        self.linearrange = self.linearmax - self.linearmin

        return True

    def formatgraph(self, G):
        if len(G.edges()) < 1:
            return pd.DataFrame()

        ## this creates a popped dataframe in u, v, d1, d2, d3... where dX are the data attributes
        popped_df_from_edges = edges_to_dataframe(G)

        ## step 2, extract the xs and ys for the nodes listed in the dataframe.
        # 2a, make a df of the nodes with the rt and mz
        df_nodes = nodes_to_dataframe(G, ['rt', 'mz'])

        # 2b, join by intersection the df_nodes onto the popped_df_from_edges,
        # using as index firstly u and then v.
        df_all_edges_with_origin = pd.concat(
            [popped_df_from_edges.set_index('u'), df_nodes.rename(columns={'rt': 'rt1', 'mz': 'mz1'})],
            axis=1, join='inner')

        # to use the v column as index, I need to first reset the index back into the df.
        df_all_edges_with_origin['u'] = df_all_edges_with_origin.index

        # then, do concat with intersection based on the v node index.
        df_all_edges_with_origin_dest = pd.concat([df_all_edges_with_origin.set_index('v'),
                                                   df_nodes.rename(columns={'rt': 'rt2',
                                                                            'mz': 'mz2'
                                                                            })],
                                                  axis=1, join='inner')

        df_all_edges_with_origin_dest = pd.DataFrame(df_all_edges_with_origin_dest).rename(
            columns={'delta_m/z': 'delta_mz'})

        # and again, re-include the index as column v
        df_all_edges_with_origin_dest['v'] = df_all_edges_with_origin_dest.index

        """
        here i need tomake sure that the signs are all correct!
        i define the gain of a glycan as the positive direction.
        """
        # flip delta_RT if the signed delta_mz is negative
        df_all_edges_with_origin_dest['signed_delta_mz'] = \
            (df_all_edges_with_origin_dest['mz2'] - df_all_edges_with_origin_dest['mz1'])
        # if the delta_mz is negative, then I need to flip the sign of the delta_RT
        df_all_edges_with_origin_dest['gain_of_glycan_delta_rt'] = \
            (df_all_edges_with_origin_dest['rt2'] - df_all_edges_with_origin_dest['rt1']) * np.sign(
                df_all_edges_with_origin_dest['signed_delta_mz'])

        # finally replacing with integer index
        pd.DataFrame(df_all_edges_with_origin_dest).reset_index(inplace=True, drop=True)

        # and then combining the mz and rt together as pairs for the line drawing.
        df_all_edges_with_origin_dest['xs'] = [list(a) for a in zip(df_all_edges_with_origin_dest['rt1'],
                                                                    df_all_edges_with_origin_dest['rt2'])]
        df_all_edges_with_origin_dest['ys'] = [list(a) for a in zip(df_all_edges_with_origin_dest['mz1'],
                                                                    df_all_edges_with_origin_dest['mz2'])]

        # then, cut out the unused columns
        df_all_edges_with_origin_dest = pd.DataFrame(df_all_edges_with_origin_dest).drop(
            labels=['hypotenuse', 'rt1', 'rt2', 'mz1', 'mz2', 'u', 'v'], axis=1)

        return df_all_edges_with_origin_dest

    def scale_intensity(self, df, mode=1):
        try:
            if not isinstance(df, pd.Series):
                df = pd.Series(df)
        except:
            print('Error. scaleintensity input could not be converted to a pandas Series')
            return df
        # dfx['intensity_scaled'] = ((np.log10(dfx['intensity']) - 6) / 6) ** (2) * (circle_size_max - circle_size_min) + circle_size_min

        if mode == 0:
            # scales data that occurs within 1e6 and 1e12
            return ((np.log10(df) - 6) / 6) ** (2)

        elif mode == 1:
            df = np.log10(df)
            # this gets the upper 75% threshold
            # q = df.quantile(q=0.25, interpolation='midpoint')
            # below the q threshold, transform all to the miniumum value, i.e. q
            df[df <= self.percentile75] = self.percentile75
            # normalise by min and max to make it over [0,1]
            df = (df - self.percentile0) / (self.logrange)
            return (df)

        elif mode == 2:
            # this mode sets it to the 15% to 85% mark
            df = np.log10(df)
            #            qmin = df.quantile(q=0.15, interpolation='midpoint')
            #            qmax = df.quantile(q=0.85, interpolation='midpoint')
            df[df <= self.percentile15] = self.percentile15
            df[df >= self.percentile85] = self.percentile85
            df = (df - self.percentile0) / (self.logrange)
            return (df)

        elif mode == 3:
            # this just does a simple normalisation after log
            df = np.log10(df)
            df = (df - self.percentile0) / (self.logrange)
            return df

        elif mode == 4:
            # linear scaling
            df = (df - self.linearmin) / (self.linearrange)
            return df


    def update_hovertool_with_glycan_list(self, abbrevs_to_display_list, glycan_column_names_list, jumplist_column_names):
        """Finds the Hovertool in the plot, and update the Composition value with a user supplied list of glycan abbreviations and column names.

        The column names must correspond to the ColumnDataSource, i.e. what the nodes are labeled with.

        :param abbrevs_to_display_list:
        :param glycan_column_names_list:
        :return:
        """

        # use this to detect which tool in self.p.tools is the HoverTool
        for tool_index, tool in enumerate(self.p.tools):
            if isinstance(self.p.tools[tool_index], HoverTool):
                break
        else:
            print('Could not detect the HoverTool in the plot. Exiting update_hovertool_with_glycan_list function.')
            return

        # format the composition display name as ('name', 'HexNAc@column_name')
        custom_tooltip_value = ''
        for abbrev, colname in zip(abbrevs_to_display_list,glycan_column_names_list):
            custom_tooltip_value += '{}@{{{}}} '.format(abbrev, colname)

        # do the same for the edges
        edge_custom_tooltip_value = ''
        for abbrev_edge, colname_edge in zip(abbrevs_to_display_list, jumplist_column_names):
            edge_custom_tooltip_value += '{}@{{{}}} '.format(abbrev_edge, colname_edge)

        # this gets the list of tuples of (displayed name, value) from an existing HoverTool
        tooltip_list_of_tuples = tool.tooltips

        # find and replace the Composition field in the tuple, while retaining the other tooltips
        new_tooltip_list_of_tuples = []
        for tooltip_index, (tooltip_name, tooltip_value) in enumerate(tooltip_list_of_tuples):
            if tooltip_name == 'Composition':
                new_tooltip_list_of_tuples.append((tooltip_name, custom_tooltip_value))
            else:
                new_tooltip_list_of_tuples.append((tooltip_name,tooltip_value))

        # add on an extra line for the Edge Compositions
        new_tooltip_list_of_tuples.append(('Edge', edge_custom_tooltip_value))

        # apply the new tooltips to the hover tool
        self.p.tools[tool_index] = HoverTool(tooltips=new_tooltip_list_of_tuples)

        print('Updated the HoverTool with:\n{}'.format(new_tooltip_list_of_tuples))

        return


class convert_xlsx_to_graphml:

    def __init__(self):
        self.resultlist = []
        filelist = custom_tools.getfiles('choose xlsx', '.xlsx')

        for f in filelist:
            df = pd.read_excel(f, index_col='node')

            G = dataframe_to_nodes(df)

            outfile = os.path.splitext(f)[0] + '.graphml'
            nx.write_graphml(G, path=outfile)
            self.resultlist.append(outfile)


def parseoptions(kw):
    kw['show_edges'] = kw.get('show_edges') if kw.get('show_edges',
                                                      None) != None else 'n'  # custom_tools.singleinputbox(text='Show Edges? (y/n):', default='n')

    return dict(
        export_csv=kw.get('export_csv', False),
        circle_size_max=kw.get('circle_size_max', 40),
        circle_size_min=kw.get('circle_size_min', 5),
        show_edges=kw.get('show_edges', False),
        output_format=kw.get('output_format', 'svg'),
        style=kw.get('style', ''),
        output_html_file_path=kw.get('output_html_file_path', None),
        scaling=kw.get('scaling', 0),
    )


def parsecolors(color_list):
    out_list = []
    for coloritem in color_list:
        out_list.append(cnames[coloritem])
    return out_list


def edges_to_dataframe(G):
    data_edges = pd.DataFrame(list(G.edges(data=True)), columns=['u', 'v', 'd'])  # this creates a df as [u, v, dict]
    data_edges_data = pd.DataFrame(list(data_edges.loc[:, 'd']))  # this extracts out dict into a df of its own.
    data_merged = pd.concat([data_edges.loc[:, ['u', 'v']], data_edges_data], axis=1)

    return data_merged


def nodes_to_dataframe(G, keylist=[]):
    if keylist == []:
        # the last node is taken because the first node [position 0] is often the REFERENCE NODE
        keylist = G.node[list(G.nodes())[-1]].keys()
    dict_from_graph = {}
    for key in keylist:
        dict_from_graph[key] = nx.get_node_attributes(G, key)

    return pd.DataFrame(index=G.nodes(),
                        columns=keylist,
                        data=dict_from_graph)


def dataframe_to_nodes(df, keydict={}, use_inherent_headers=True):
    """

    :param df: is the dataframe that you wish to convert to a graph
    :param keydict: is a dictionary mapping for renaming the columns of df after importing
    :param use_inherent_headers: set to true to use the columns of the dataframe being inputted
    :return:
    """

    keydict = keydict or {'mz_cf': 'mz', 'rt_cf': 'rt', 'intensity_cf': 'intensity'}
    keylist = list(df.columns) if use_inherent_headers else ['mz', 'rt', 'intensity']

    # must set dtype to object, otherwise it snaps to np.float, which is unparsable by graphml writer!
    df = df.rename(columns=keydict)
    dfslice = df.loc[:, keylist].astype(dtype='object')
    print(dfslice.loc[:, keylist[-1]])

    data_dict = dfslice.to_dict(orient='index')

    G = nx.Graph()
    G.add_nodes_from(data_dict.items())

    return G


def graphmls_to_excel(graphmls=[]):
    graphmls = graphmls or custom_tools.getfiles('Pick the graphmls', '.graphml')

    G = nx.Graph()

    for g in graphmls:
        temp_graph = nx.read_graphml(path=g)
        G = nx.compose(G, temp_graph)

    dfnodes = nodes_to_dataframe(G)

    outfile = custom_tools.getsavefile('Name your output excel file', '.xlsx')
    writer = pd.ExcelWriter(outfile)
    dfnodes.to_excel(writer, 'Combined GraphMLs', index_label='node')


def graphmls_to_one(graphmls=[], savefile=''):
    graphmls = graphmls or custom_tools.getfiles('Pick the graphmls', '.graphml')

    G = nx.Graph()

    for g in graphmls:
        temp_graph = nx.read_graphml(path=g)
        G = nx.compose(G, temp_graph)

    outfile = savefile or custom_tools.getsavefile('Name your output graphml file', '.graphml')
    nx.write_graphml(G, outfile)
    return (G, outfile)


def extract_metadata_from_zeroth_node(G):
    try:
        zeroth_node_attr = G.node[0]  # checks if exists

        if (zeroth_node_attr['rt'] == 0.) and \
                (zeroth_node_attr['mz'] == 0.):
            jumplist_all = pd.read_html(zeroth_node_attr['jumplist_all'])[0]
            jumplist_cluster = pd.read_html(zeroth_node_attr['jumplist_cluster'])[0]
            jumplist = pd.read_html(zeroth_node_attr['jumplist'])[0]
        else:
            print('no zeroth node was found. returning defaults')
            if nx.get_edge_attributes(G, 'cluster') == {}:
                return uselegacygroup(), None, uselegacygroup(), True

            else:
                print('using normal glymod groupings')
                return usedefaultgroup(), None, usedefaultgroup(), False
    except:
        return uselegacygroup(), None, uselegacygroup(), True
    #        # find the node with mz=rt=0
    #        all_rt_values = nx.get_node_attributes(G, 'rt')
    #        all_mz_values = nx.get_node_attributes(G, 'mz')
    #        if (0. in all_rt_values.values()) and \
    #            (0. in all_mz_values.values()):

    # print('Outputting zeroth node...')
    return jumplist_all, jumplist_cluster, jumplist, False


def usedefaultgroup():
    return pd.DataFrame({'cluster': {0: 0, 1: 0, 2: 0, 3: 0, 4: 1, },
                         'cluster_min_rt': {0: 0, 1: 0, 2: 0, 3: 0, 4: 300, },
                         'color': {0: 'blue',
                                   1: 'red',
                                   2: 'green',
                                   3: 'yellow',
                                   4: 'pink'},
                         'difference': {0: 162.05282,
                                        1: 203.07936999999998,
                                        2: 365.13218999999998,
                                        3: 146.05791000000002,
                                        4: 291.09539999999998},
                         'glymod_F': {0: 0, 1: 0, 2: 0, 3: 1, 4: 0, },
                         'glymod_H': {0: 1, 1: 0, 2: 1, 3: 0, 4: 0, },
                         'glymod_N': {0: 0, 1: 1, 2: 1, 3: 0, 4: 0, },
                         'glymod_S': {0: 0, 1: 0, 2: 0, 3: 0, 4: 1, },
                         'glymod_mod1': {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, },
                         'glymod_mod2': {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, },
                         'glymod_mod3': {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, },
                         'glymod_mod4': {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, },
                         'max_rt': {0: 50, 1: 50, 2: 70, 3: 50, 4: 500, },
                         'weight': {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, }}
                        )


def uselegacygroup():
    # print('using legacy groupings')
    return pd.DataFrame({
        'F': {0: 1, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0},
        'H': {0: 0, 1: 1, 2: 0, 3: 1, 4: 0, 5: 1},
        'N': {0: 0, 1: 0, 2: 1, 3: 1, 4: 0, 5: 1},
        'S': {0: 0, 1: 0, 2: 0, 3: 0, 4: 1, 5: 1},
        'cF': {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1},
        'cH': {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1},
        'cN': {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1},
        'cS': {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1},
        'color': {0: 'red',
                  1: 'gold',
                  2: 'blue',
                  3: 'green',
                  4: 'pink',
                  5: 'fuchsia'},
        'name': {0: 'fuc', 1: 'hex', 2: 'hexnac', 3: 'HN', 4: 'neuac', 5: 'SHN'}}
    )


def removezerothnode(G):
    G.remove_nodes_from([0])
    return G


def getintensitylist(graph_file):
    G = nx.read_graphml(path=graph_file, node_type=int)
    return list(nx.get_node_attributes(G, 'intensity').values())


def get_intensity_and_rt_from_graphml(graph_file):
    G = nx.read_graphml(path=graph_file, node_type=int)
    return list(nx.get_node_attributes(G, 'intensity').values()), list(nx.get_node_attributes(G, 'rt').values())


class collect_stats:
    def __init__(self):
        self.G = nx.Graph()
        self.extract_data_from_graphmls()

    def extract_data_from_graphmls(self):
        """
        get a list of graphmls, if list, combine them into one.
        If one file, no need to combine.
        match it to the xs and ys
        pop out the xs and ys to x1x2, y1y2

        split the df into the different hex, hexnac, neuac etc
        draw histograms
        export as different sheets

        :return:
        """

        file_list = custom_tools.getfiles('select one or more graphml files', '.graphml')
        self.G = self.combine_graphmls_if_list(file_list_in=file_list)
        df = subgraphfigure.formatgraph(1, self.G)
        # pop the xs and ys
        df['x1'] = df['xs'].apply(lambda x: x[0])
        df['x2'] = df['xs'].apply(lambda x: x[1])
        df['y1'] = df['ys'].apply(lambda x: x[0])
        df['y2'] = df['ys'].apply(lambda x: x[1])

        self.df = df
        self.hex = df.loc[(df['glymod_H'] == 1) & (df['glymod_N'] == 0), :]
        self.hexnac = df.loc[(df['glymod_H'] == 0) & (df['glymod_N'] == 1), :]
        self.fuc = df.loc[(df['glymod_F'] == 1) & (df['glymod_N'] == 0), :]
        self.neuac = df.loc[(df['glymod_S'] == 1), :]
        self.lacnac = df.loc[(df['glymod_H'] == 1) & (df['glymod_N'] == 1), :]

    def combine_graphmls_if_list(self, file_list_in):
        if isinstance(file_list_in, tuple):
            if len(file_list_in) > 1:
                G, outfile = graphmls_to_one(file_list_in)
            else:
                # is a list, but there's only one entry
                G = nx.read_graphml(file_list_in[0])
        else:
            # not a list.
            G = nx.read_graphml(file_list_in)

        return G

    def plot_histograms(self):
        """
        plt.figure(figsize=(10,5))
        plt.rcParams.update({'font.size': 22})
        plt.hist2d(c.hex['x1'], c.hex['gain_of_glycan_delta_rt'], bins=[50,30], cmap='magma')
        plt.show()

        custom_tools.reload(bk)
        s = bk.collect_stats()
        s.plot_histograms()
        """

        glycan_list_string = ['hex',
                              'hexnac',
                              'fuc',
                              'lacnac',
                              'neuac']

        self.stat_list = [self.hex, self.hexnac, self.fuc, self.lacnac, self.neuac]
        self.histo2d_dict = {}
        self.histo1d_dict = {}

        hist_figs, glycan_list_obj = plt.subplots(nrows=len(glycan_list_string), ncols=2, sharex=False)
        hist_figs.set_size_inches(3.9, 7.4)

        # i should find the top 5 populous bins and display the 1D histogram at that bin.
        # note that the x axis has a greater number of bins (50) than the yaxis (30)

        # nb: i could use a list here [hex, hexnac, fuc, neuac] to cycle through them easier
        """
        array.argsort() arranges from low to high
        array.argsort()[-3:] gets top 3
        array.argsort()[-3:][::-1] gets top 3, then reverses the order to now have the indices of top 3, descending.
        """

        for current_plot, current_glycan_name, current_self_stat in list(zip(glycan_list_obj,
                                                                             glycan_list_string,
                                                                             self.stat_list)):

            current_plot[0].set_title(current_glycan_name)
            self.histo2d_dict[current_glycan_name] = current_plot[0].hist2d(current_self_stat['x1'],
                                                                            current_self_stat[
                                                                                'gain_of_glycan_delta_rt'],
                                                                            bins=[50, 30],
                                                                            cmap='magma')

            # get top 5 bins
            histo_sums = np.sum(self.histo2d_dict[current_glycan_name][0], axis=1)
            top_bins_indices = histo_sums.argsort()[-3:][::-1]
            top_data_arrays = self.histo2d_dict[current_glycan_name][0][top_bins_indices]

            # add these points as a vertical bar chart
            for i, current_top_values in enumerate(top_data_arrays):
                current_plot[1].bar(left=self.histo2d_dict[current_glycan_name][2][:-1],
                                    height=current_top_values,
                                    label=str(i)
                                    )

            # current_plot[1].legend(loc='upper right')
            self.histo1d_dict[current_glycan_name] = current_plot[1]

        # add big invisible subplot to create common axis labels
        hist_figs.add_subplot(111, frameon=False)
        plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
        plt.grid(False)
        plt.xlabel('RT position in run (s)')
        plt.ylabel('delta RT when adding glycan')
        hist_figs.tight_layout(h_pad=0, w_pad=0)

        self.hist_figs = hist_figs
        self.hist_figs.show()

        pass


def runwithdefaultparameters(graphmllist=[]):
    q = subgraphfigure()
    options = dict(
        export_csv=False,
        circle_size_max=20,
        circle_size_min=2,
        show_edges=False,
        output_format='svg',
        style='',
        output_html_file_path='test.html',
        scaling=3,
    )
    q.add_subgraphs_to_plot(graphmllist, options)


def runwithalignementparameters(graphmllist=[]):
    q = subgraphfigure(has_alignment_data=True)
    options = dict(
        export_csv=False,
        circle_size_max=20,
        circle_size_min=2,
        show_edges=True,
        output_format='canvas',
        style='',
        output_html_file_path=None,
        scaling=2
    )
    q.add_subgraphs_to_plot(graphmllist, options)


def runwithsizedgraphs(graphmllist=[]):
    q = subgraphfigure()
    q.p.x_range = Range1d(0, 3000)
    q.p.y_range = Range1d(0, 6000)
    options = dict(
        export_csv=False,
        circle_size_max=15,
        circle_size_min=2,
        show_edges=False,
        output_format='canvas',
        style='',
        output_html_file_path=None,
        scaling=3,
    )
    q.add_subgraphs_to_plot(graphmllist, options)


def runwithsizedalignedgraphs(graphmllist=[]):
    q = subgraphfigure(has_alignment_data=True)
    q.p.x_range = Range1d(0, 3000)
    q.p.y_range = Range1d(0, 6000)
    options = dict(
        export_csv=False,
        circle_size_max=30,
        circle_size_min=2,
        show_edges=False,
        output_format='svg',
        style='',
        output_html_file_path=None,
        scaling=3,
    )
    q.add_subgraphs_to_plot(graphmllist, options)


def default():
    q = subgraphfigure()
    options = dict(
        export_csv=False,
        circle_size_max=20,
        circle_size_min=2,
        show_edges=True,
        output_format='svg',
        style='',
        output_html_file_path='test.html',
        scaling=3,
    )
    q.add_subgraphs_to_plot([], options)


def default_png():
    q = subgraphfigure(has_alignment_data=True)
    options = dict(
        export_csv=False,
        circle_size_max=40,
        circle_size_min=5,
        show_edges=True,
        output_format='canvas',
        style='',
        output_html_file_path='test.html',
        scaling=2,
    )
    q.add_subgraphs_to_plot([], options)

if __name__ == "__main__":
    default_png()



#    f = 'O:/Analytics/Matt/030 files for graph theory/50 prediction of total tripleETD/subgraphs64_degree5.graphml' #custom_tools.getfile('choose graphml file', '.graphml')

#    runwithdefaultparameters()
#    runwithalignementparameters()
#    runwithsizedgraphs()
#    runwithsizedalignedgraphs()


