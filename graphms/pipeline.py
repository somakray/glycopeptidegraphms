"""
This is the pipeline file which summarises all the steps that are taken to get to a completed dataset.
More importantly, it will organise files in a structured manner!
And record the steps that were taken.

Inputs:
(1) Consensusxml
(2) byonic PSM
(3) graphMS files

Major steps:
A. align byonic to csv
B. byonic graphmscrossref
C. plotting

"""
from graphms import custom_tools, alignbyonictocsv, byonicgraphmscrossref, applyindirectcompositions, writenodeid, graphquant, bokehgraphms
from graphms.alignbyonictocsv import execute
import os, shutil
from collections import OrderedDict
from shutil import copy, rmtree
from datetime import datetime
from threading import Thread

def align_byonic_to_consensusxml():
    execute()

def create_folder_system_for_datasets():
    pass

class datapipeline:
    def __init__(self, **kwargs):
        """This is the main class that holds all the filenames in memory so we don't keep prompting. And also organises the folders. To test this feature, run ``graphms.exampledatapipeline.py``

        :param kwargs: See description

        ``Keyword Arguments``
            * `top_folder`: (``string``) path to a directory (will create if nonexistent). Defaults to prompting the user with a dialogbox
            * `overwrite`: (``boolean``) if True will delete the top_folder and recreate it, otherwise if False will simply attempt to overwrite it. Defaults to True
            * `consensusxml`: (``string``) path to the consensusxml.csv file with the features (rt,mz,intensity). Defaults to prompting the user with a dialogbox
            * `byonic_psms`: (``string``) path to the byonic exported peptide sequence match (PSM) csv file. Defaults to prompting the user with a dialogbox
            * `graphms_files`: (``tuple``) of string paths to the graphml files to be examined. Defaults to prompting the user with a dialogbox
            * `predcomp_file`: (``string``) path to the predicted composition .csv file produced by graphms.predictcompositions.py. This file should have been generated on the same files as supplied in the `graphms_files` parameter. Defaults to prompting the user with a dialogbox
            * `reference_file`: (``string``) path to the reference node .xlsx file produced by graphms.predictcompositions.py. This file should have been generated on the same files as supplied in the `graphms_files` parameter. Defaults to prompting the user with a dialogbox

        """
        self.keywords = kwargs
        self.create_folder_structure()
        self.execute_data_pipeline()
        self.plot_final_results()
        custom_tools.opendir(self.top_folder)
        return


    def create_folder_structure(self):
        """
        First step is to create a top folder and create the subfolders around it.
        """
        # check if top_folder was passed into the class, and use that or prompt the user.
        self.top_folder = self.keywords.get('top_folder', '') or \
                                custom_tools.getfolder('Create, name and select a top-level folder')

                # end if no folder provided. Otherwise folder will be produced in the wrong place!
        if self.top_folder == '':
            raise NotADirectoryError('No top directory was provided. Quitting.')

        # create the folder if not present, and clear it if overwrite was not specified as false
        if os.path.exists(self.top_folder):
            if self.keywords.get('overwrite', True) == True:
                shutil.rmtree(self.top_folder)
            os.mkdir(self.top_folder)
        else:
            os.mkdir(self.top_folder)


        # create log file
        self.log_path = self.create_logfile(self.top_folder)

        self.folders_dict = self.initialise_subfolder_names()
        self.folders_dict_absolute_paths = self.create_folder_system_for_datasets(self.top_folder, self.folders_dict)

        self.simple_input_paths = self.get_simple_inputs_paths()

        self.file_folder_mapping_list = [(0, 0), (1, 0), (2, 1), (3, 1), (4, 1)]
        self.validate_file_folder_mapping(self.file_folder_mapping_list)
        # update file paths after you copy them to the new folders
        self.simple_input_paths = self.copy_input_files_to_folder(ordered_dict_of_input_file_paths=self.simple_input_paths,
                                                                  ordered_dict_of_folders_to_copy_to=self.folders_dict_absolute_paths,
                                                                  mapping_list_of_tuples_inputs_to_folders=self.file_folder_mapping_list)
        self.simple_output_paths = self.define_output_filepaths(folders_dict_absolute_paths=self.folders_dict_absolute_paths)


        self.update_logfile('File structure created.')

        return


    """
    These two functions define the files and folders.
    """

    def initialise_subfolder_names(self):
        """
                this makes the different folders
                :return:
                """
        # use a dict because this makes it easier to modify in the future using some sort of customisable
        # text parameter file in the future.
        # and I can iterate through this as well.
        # using an OrderedDict to maintain the order.

        return OrderedDict(input_simple_subfolder_name='101_simple_inputs',
                                   input_graphms_subfolder_name='102_graphms_inputs',
                                   output_alignment_subfolder_name='201_alignment_outputs',
                                   output_identifications_subfolder_name='202_identification_outputs',
                                   output_plots_subfolder_name='301_plots',
                                   )


    def get_simple_inputs_paths(self):
        """get the user input for all input files, or use what keyword arguments were supplied

        :return:
        """
        simple_inputs_paths = OrderedDict(consensusxml = self.keywords.get('consensusxml','') or custom_tools.getfile('Select the consensusxml csv file', '.csv'),
                                          byonic_psms= self.keywords.get('byonic_psms','') or custom_tools.getfile('Select the byonic PSM csv file', '.csv'),
                                          graphms_files= self.keywords.get('graphms_files','') or custom_tools.getfiles('Select the graphMS files', '.graphml'),
                                          predcomp_file= self.keywords.get('predcomp_file','') or custom_tools.getfile('Select the GraphMS predicted relative compositions file', '.csv'),
                                          reference_file= self.keywords.get('reference_file','') or custom_tools.getfile(
                                              'Select the GraphMS outputted reference nodes file', '.xlsx'),
                                          )

        msg = [self.update_logfile('Inputs chosen by user are: {}={}'.format(x, simple_inputs_paths[x])) for x in simple_inputs_paths]

        return simple_inputs_paths

    def get_simple_inputs_paths_test(self, test_dict={}):
        if test_dict == {}:
            simple_inputs_paths = OrderedDict(consensusxml = r'O:\Analytics\Matt\042 EPO christine\May_2019\graphms on cho,hek\CHO consensusxml.csv',
                                          byonic_psms=r'O:\Analytics\Matt\042 EPO christine\May_2019\may 2019, 301 CHO EPO, byonic psm r1-0.csv',
                                          graphms_files=('O:/Analytics/Matt/042 EPO christine/May_2019/graphms on cho,hek/cho graph2/0neuacmerged[4, 22, 8, 133, 50, 12, 68, 38, 60, 7, 16].graphml',
                                                         'O:/Analytics/Matt/042 EPO christine/May_2019/graphms on cho,hek/cho graph2/1neuacmerged[6, 0, 47, 25, 49, 35, 5, 21, 61].graphml',
                                                         'O:/Analytics/Matt/042 EPO christine/May_2019/graphms on cho,hek/cho graph2/2neuacmerged[2, 1, 99, 13, 3].graphml',
                                                         ),
                                          predcomp_file=r'O:\Analytics\Matt\042 EPO christine\May_2019\graphms on cho,hek\cho graph2\cho_predcomp_graph2_2101out.csv',
                                          reference_file=r'O:\Analytics\Matt\042 EPO christine\May_2019\graphms on cho,hek\cho graph2\cho_predcomp_graph2_reference_nodes_2101out.xlsx',
                                          )
        else:
            simple_inputs_paths = test_dict

        msg = [self.update_logfile('TEST Inputs chosen by user are: {}={}'.format(x, simple_inputs_paths[x])) for x in simple_inputs_paths]

        return simple_inputs_paths

    def define_output_filepaths(self, folders_dict_absolute_paths):
        output_filepaths = OrderedDict(aligned_byonic_filepath=os.path.join(folders_dict_absolute_paths['output_alignment_subfolder_name'], '01_byonic_aligned_to_csv.xlsx'),
                             byoniccrossref_filepath=os.path.join(folders_dict_absolute_paths['output_alignment_subfolder_name'], '02_byonic_crossref_matched_reference_nodes.xlsx'),
                             final_absolute_compositions_filepath=os.path.join(folders_dict_absolute_paths['output_identifications_subfolder_name'], '03_absolute_compositions_graphms_x_byonic_crossref.xlsx'),
                             final_peptide_quant_filepath='',
                             new_graphml_files_with_nodeids_list=[],
                             bokeh_html_filepath=os.path.join(folders_dict_absolute_paths['output_plots_subfolder_name'], '501_bokeh_plot_with_nodeids.html'),
                                       bokeh_html_idsonly_filepath=os.path.join(folders_dict_absolute_paths['output_plots_subfolder_name'], '502_bokeh_plot_with_nodeids_only_identified_subgraphs.html'),
                             peptide_quant_bokeh_html_filepath=os.path.join(folders_dict_absolute_paths['output_plots_subfolder_name'], '503_bokeh_plot_of_peptide_quants.html'),
                             peptide_quant_scaled_values_filepath=os.path.join(folders_dict_absolute_paths['output_plots_subfolder_name'], '504_scaled_values_used_for_peptide_quants.xlsx')
                             )

        return output_filepaths


    def execute_data_pipeline(self):
        """
        Steps:
        A. align byonic to csv
        B. byonic graphmscrossref
        C. plotting
        :return:
        """

        """
        for align byonic to csv, use the dict keys:
                        
        """
        self.update_logfile('Started to execute data pipeline...')

        self.execute_align_byonic_to_csv(savedir=self.folders_dict_absolute_paths['output_alignment_subfolder_name'],
                                         consensus_filepath=self.simple_input_paths['consensusxml'],
                                         byonic_psm_filepath=self.simple_input_paths['byonic_psms'],
                                         save_excel_file_name=self.simple_output_paths['aligned_byonic_filepath']
                                         )

        byonic_crossref_save_path = byonicgraphmscrossref.run_with_defined_filepaths(aligned_file=self.simple_output_paths['aligned_byonic_filepath'],
                                                         ref_file=self.simple_input_paths['reference_file'],
                                                         predcomp_file=self.simple_input_paths['predcomp_file'],
                                                         save_path=self.simple_output_paths['byoniccrossref_filepath'],
                                                         )
        # do logging
        self.update_logfile('Byonic aligned file: "{}" was cross referenced with GraphMS relative nodes: "{}" and the matching IDed reference file was created from the auto-generated reference node file: "{}". Final IDed reference file was at "{}".'.format( \
            self.simple_output_paths['aligned_byonic_filepath'],
            self.simple_input_paths['predcomp_file'],
            self.simple_input_paths['reference_file'],
            self.simple_output_paths['byoniccrossref_filepath'],))

        """ 
        :param kw: relativecompfile as predcomp.csv, referencefile as refnodes.xlsx, savefile as abscompout.xlsx
        
        the result from "applyindirectcompositions.applyindirectcompositions" is a dictionary of:
                dict(resultpath=os.path.splitext(savefile)[0]+'.xlsx',
                  resultdir=os.path.dirname(savefile),
                  quantpath=os.path.splitext(savefile)[0]+'_outPepQuant.xlsx',
                  )
        """
        apply_indirect_compositions_result_dict = \
            applyindirectcompositions.applyindirectcompositions(relativecompfile=self.simple_input_paths['predcomp_file'],
                                                                referencefile=self.simple_output_paths['byoniccrossref_filepath'],
                                                                savefile=self.simple_output_paths['final_absolute_compositions_filepath'])

        # this stores the path to the peptide quantitation .xlsx file.
        self.simple_output_paths['final_peptide_quant_filepath'] = apply_indirect_compositions_result_dict['quantpath']

        # do logging
        self.update_logfile('From the relative compositions file: "{}" and the IDed reference file: "{}", this script created the final absolute compositions file: "{}" and the peptide quantitation file: "{}"'.format(
            self.simple_input_paths['predcomp_file'],
            self.simple_output_paths['byoniccrossref_filepath'],
            self.simple_output_paths['final_absolute_compositions_filepath'],
            self.simple_output_paths['final_peptide_quant_filepath']
        ))

        return


    def plot_final_results(self):
        """
        To finish the process, we visualise the data, as follows:
            plot the LABELED GraphMS plot.
            plot the quant of the peptide glycoforms

        :return:
        """

        """
        1a) Plot the now labeled GraphMS graphs
        Do the explicit plotting with parameters defined:
        graphoptions=
            dict(
                export_csv=kw.get('export_csv', False),
                circle_size_max=kw.get('circle_size_max', 40),
                circle_size_min=kw.get('circle_size_min', 5)  ,  
                show_edges=kw.get('show_edges', False),
                output_format=kw.get('output_format', 'svg'),
                style=kw.get('style', ''),
                output_html_file_path=kw.get('output_html_file_path', None),
                scaling=kw.get('scaling',0),                       
                ) 
        """
        self.update_logfile('Updating the IDs of the GraphMS files...')

        # this generates the list of new graphml files with nodeids written in.
        list_of_new_graphml_saved_filepaths = writenodeid.run_with_defined_inputs_and_outputs_no_plotting(abscomp_filepath=self.simple_output_paths['final_absolute_compositions_filepath'],
                                                                                                          graphs_tuple=self.simple_input_paths['graphms_files'],
                                                                                                          savedir=self.folders_dict_absolute_paths['output_plots_subfolder_name'],
                                                                                                          )
        # update the class' property with the new saved graphml files.
        self.simple_output_paths['new_graphml_files_with_nodeids_list'] = list_of_new_graphml_saved_filepaths

        # do logging
        self.update_logfile('Finished. GraphMS files were updated with node ID from absolute composition file "{}" and were saved as the list: {}'.format(self.simple_output_paths['final_absolute_compositions_filepath'],
                                                                                                                                                self.simple_output_paths[
                                                                                                                                                    'new_graphml_files_with_nodeids_list']))
        self.update_logfile('Plotting final data...')
        p = bokehgraphms.subgraphfigure(has_alignment_data=True)
        graphoptions = {'scaling': 1,
                                              'show_edges': True,
                                              'output_format': 'canvas',
                                              'output_html_file_path': self.simple_output_paths['bokeh_html_filepath'],
                                              'circle_size_max':20,
                                              'circle_size_min':5,
                                              }
        p.add_subgraphs_to_plot(graphmls=list_of_new_graphml_saved_filepaths,
                                graphoptions=graphoptions)

        # plot a second graph with only those graphs that were identified

        p2 = bokehgraphms.subgraphfigure(has_alignment_data=True)
        graphoptions = {'scaling': 1,
                        'show_edges': True,
                        'output_format': 'canvas',
                        'output_html_file_path': self.simple_output_paths['bokeh_html_idsonly_filepath'],
                        'circle_size_max': 20,
                        'circle_size_min': 5,
                        }
        p2.add_subgraphs_to_plot(graphmls=list_of_new_graphml_saved_filepaths,
                                graphoptions=graphoptions, filename_filter_to_exclude = 'unknown')

        # do logging
        self.update_logfile('Plot was created as: "{}" with the options: {}'.format(self.simple_output_paths['bokeh_html_filepath'], graphoptions))



        """
        2) Plot the Peptide Quant         
        """
        result_dict_saved_filepaths = graphquant.graphquant(
            quant_file=self.simple_output_paths['final_peptide_quant_filepath'],
                              threshold_percent=0.,
                              sort_by_neuac=False,
                              sort_by_antenna=False,
                              save_filename=self.simple_output_paths['peptide_quant_bokeh_html_filepath'],
                              scaled_values_save_filename=self.simple_output_paths['peptide_quant_scaled_values_filepath'],
                              )

        # do logging
        self.update_logfile('Peptide Quantitation data input file: "{}" was plotted as a graph at "{}"'.format(
            self.simple_output_paths['final_peptide_quant_filepath'],
            self.simple_output_paths['peptide_quant_bokeh_html_filepath']
        ))

        return list_of_new_graphml_saved_filepaths, result_dict_saved_filepaths


    def execute_align_byonic_to_csv(self, savedir, consensus_filepath,
                                    byonic_psm_filepath, save_excel_file_name,
                                    ):

        """
        align byonic to csv

        returns dict(alignbyonictocsv_object=result,
                saved_aligned_excel_file_name=save_excel_file_name)
        """
        alignbyonictocsv_object, saved_aligned_excel_file_name = \
            alignbyonictocsv.execute_on_single_pair_of_files_with_defined_inputs_return_result_dict(
                savedir=savedir,
                consensus_filepath=consensus_filepath,
                byonic_psm_filepath=byonic_psm_filepath,
                save_excel_file_name=save_excel_file_name,
                rtcorrection=0,
                rt_tol_in_seconds=60,
        )

        self.update_logfile('Byonic PSMs: "{}" and Consensusxml: "{}" were aligned; output file: "{}"'.format(
            byonic_psm_filepath,
            consensus_filepath,
            saved_aligned_excel_file_name,
        ))

        return



    def get_time_now_as_string(self):
        now = datetime.now()
        return now.strftime("%d/%m/%Y, %H:%M:%S.%f")


    def create_logfile(self, top_folder):
        log_file_path = os.path.join(top_folder, 'logfile.txt')
        with open(log_file_path, 'a+') as f:
            f.write('Logging began at {}\n'.format(self.get_time_now_as_string()))

        return log_file_path

    def update_logfile(self, log_message):
        with open(self.log_path, 'a') as f:
            temp_log_message = '{} >> {}\n'.format(self.get_time_now_as_string(),
                                                 log_message)
            f.write(temp_log_message)
            print(temp_log_message)
        return log_message





    def create_folder_system_for_datasets(self, top_folder, folders_dict):
        """
        This creates the folder and then defines a new OrderedDict with the
        absolute paths, for later reference.

        I'm adding in the inputs explicitly instead of relying on self.variables
        to improve modularity and testing.
        :param top_folder:
        :param folders_dict:
        :return:
        """
        self.update_logfile('Started to create folders under "{}"...'.format(top_folder))
        # initialise absolute paths.
        folders_dict_absolute_paths = OrderedDict()




        for f in folders_dict:
            target_folder = folders_dict[f]
            abspath = os.path.join(top_folder, target_folder)
            try:
                os.mkdir(abspath)
                self.update_logfile('Folder created at {}'.format(abspath))
            except FileExistsError as e:
                message = ('Directory already contains folders! Cannot Overwrite. Error=', e)
                print(message)
                self.update_logfile(message)
                return

            folders_dict_absolute_paths[f] = abspath

        self.update_logfile('Finished. Folders were created under top folder of "{}"'.format(top_folder))

        return folders_dict_absolute_paths



    def validate_file_folder_mapping(self, mapping_list):
        # check if files are all covered.
        check_files_serialised = [x for x,y in mapping_list]==list(range(len(mapping_list)))
        if not check_files_serialised:
            error_message = 'The mapping list of tuples used to map the copying of the input files to the new folders does not cover running numbers: Mapping = {}'.format(mapping_list)
            self.update_logfile(error_message)
            raise ValueError(error_message)

        return check_files_serialised


    def copy_input_files_to_folder(self, ordered_dict_of_input_file_paths, ordered_dict_of_folders_to_copy_to, mapping_list_of_tuples_inputs_to_folders):
        """
        This uses a tuple map (file index, folder index) to send the files to the correct folders. This
        allows customisation in the future.
        an example is [(1,1),(2,1),(3,2)] to send files 1 and 2 to folder 1, and
        file 3 to folder 2.
        :param ordered_dict_of_input_file_paths:
        :param ordered_dict_of_folders_to_copy_to:
        :param mapping_list_of_tuples_inputs_to_folders:
        :return:
        """

        # initialise a new OrderedDict of the new absolute paths for the files
        new_dict_of_absolute_paths = OrderedDict()

        for (input_file_index, output_folder_index) in mapping_list_of_tuples_inputs_to_folders:
            input_file_path = ordered_dict_of_input_file_paths[list(ordered_dict_of_input_file_paths.keys())[input_file_index]]
            output_folder_path = ordered_dict_of_folders_to_copy_to[list(ordered_dict_of_folders_to_copy_to.keys())[output_folder_index]]
            print(input_file_path,output_folder_path)
            if isinstance(input_file_path, tuple) or isinstance(input_file_path, list):
                for individual_file in input_file_path:
                    copied_file = copy(individual_file, output_folder_path)
                    self.update_logfile('File copied from "{}" to "{}"'.format(individual_file, copied_file))
            else:
                copied_file = copy(input_file_path, output_folder_path)
                self.update_logfile('File copied from "{}" to "{}"'.format(input_file_path, copied_file))

            new_dict_of_absolute_paths[list(ordered_dict_of_input_file_paths.keys())[input_file_index]] = input_file_path

        print(new_dict_of_absolute_paths)
        return new_dict_of_absolute_paths




if __name__ == '__main__':
    d = datapipeline()
