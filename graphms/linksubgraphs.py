"""
this imports a set of graphmls, links those that are connected by NeuAc
then exports new graphmls into a new folder with the correct groupings.

Example how to run this:
this is how I would call it
k = linksub()
k.execute()
print(k.graph_list)
"""




import numpy as np
import pandas as pd
import networkx as nx
from graphms import custom_tools,glymod_functions
import os
import re


#from collections import namedtuple, OrderedDict
from threading import Thread


def nodes_to_dataframe(G, keylist=[]):
    if keylist == []:
        # the last node is taken because the first node [position 0] is often the REFERENCE NODE
        keylist = G.node[list(G.nodes())[-1]].keys()
    dict_from_graph = {}
    for key in keylist:
        dict_from_graph[key] = nx.get_node_attributes(G, key)

    return pd.DataFrame(index=G.nodes(),
                        columns=keylist,
                        data=dict_from_graph)
    

def creatematrix(num_items):
    # this takes in a N scalar and outputs a list of positions
    # that if followed, represent a unique, non-self pairing
#    matrix = np.subtract(np.arange(num_items),
#                         np.arange(num_items).reshape(num_items,1),)

#    out_list = []
#    for (x, data) in np.ndenumerate(matrix):
#        if data==True:
#            out_list.append(x)
#    return out_list

    # i think this may quicker:
    return np.argwhere(
            np.subtract(np.arange(num_items),
                         np.arange(num_items).reshape(num_items,1),)
            > 0)

    


class linksub:
        
    def __init__(self, graphmls = [], graphs=[], 
                 outputfolder='', threshold=0.6,
                 df_cluster=None, flag_restrict_by_rt_direction=True):
        self.outgraphlist = []
        self.inputgraphs = graphs
        self.graphmlstolink = graphmls
        self.outgraphs = []
        self.outputfolder = outputfolder
        self.threshold = threshold
        self.flag_restrict_by_rt_direction = flag_restrict_by_rt_direction
        
        # this is to set the default cluster as NeuAc
#        pdb.set_trace()
#        self.df_cluster = self.default_cluster() if df_cluster==None else df_cluster
        try:
            if df_cluster==None: self.df_cluster = self.default_cluster()
        except:
            self.df_cluster = df_cluster.reset_index()
        
        self.glymod_full_colnames_list, self.glymod_shortened_mod_name_list = \
                glymod_functions._extractglymod(self.df_cluster, 
                                                delimiter='glymod_')
        
        
        
        self.execute()        
    
    def default_cluster(self):
        print('using default cluster')
        return pd.DataFrame(
                    dict(difference=291.0954,
                         cluster_min_rt=100,
                         max_rt=500, 
                         color='pink', 
                         cluster=1,
                         glymod_S=1,
                         glymod_H=0,
                         glymod_N=0,
                         glymod_F=0, 
                         S=1,
                         H=0,
                         N=0,
                         F=0,
                         ),
                         index=[0]
                    )
        
    def get_outgraphlist(self):
        return self.outgraphlist

    def get_outgraphs(self):
        return self.outgraphs

##    def set_outgraphlist(self, graphlist):
##        self.outgraphlist = graphlist

    def set_inputgraphs(self, graphs):
        self.inputgraphs = graphs

    def get_inputgraphs(self):
        return self.inputgraphs

    def set_graphmlstolink(self, graphmls):
        self.graphmlstolink = graphmls

    def get_graphmlstolink(self):
        return self.graphmlstolink
    
    def set_threshold(self, setvalue):
        self.threshold = setvalue


    def comparesubgraphs(self, graph_one, graph_two, 
                         additive=291.0954, 
                         rt_range=[100,500], 
                         tolerance=0.02):

#        pdb.set_trace()
        self.rt_range = rt_range
        # first, extract the graphs into popped dataframes.
        keylist = graph_one.node[list(graph_one.nodes())[0]].keys()
        df_one = nodes_to_dataframe(graph_one, keylist=keylist)
        df_two = nodes_to_dataframe(graph_two, keylist=keylist)
        swap = False # sets the flag to tell me later whether I should swap them
        
        # order the df such that df_one will have the greater RT than df_two        
        if ((df_one['rt'].max() + df_one['rt'].min())/2) - ((df_two['rt'].max() + df_two['rt'].min())/2) < 0: # swap if RT difference is negative. To make df_one the right sided.
            df_one, df_two = df_two, df_one
            swap=True
         
        ## make the subtraction matrices
        # want to find the nodes where mz difference =291 and RT difference is positive.
        mask_mz_diff = np.isclose(additive,
                                  np.array([df_one['mz']]).T - np.array([df_two['mz']]),
                                  atol=tolerance,
                                  rtol=0,
                                  )
        
        if self.flag_restrict_by_rt_direction:
            mask_rt_diff = np.isclose(np.mean(rt_range),
                                      np.array([df_one['rt']]).T - np.array([df_two['rt']]),
                                      atol=(rt_range[1]-rt_range[0])/2,
                                      rtol=0,
                                      )
            matched_indices = np.where(mask_mz_diff & mask_rt_diff)            
        else:
            matched_indices = mask_mz_diff
            
        ## how to convert indices into edges?
        # first, reference all the nodes matched as pairs
        ## this is one possible idea.
#        matched_nodes_from_one = [df_one.index[i] for i in matched_indices[0]]
#        matched_nodes_from_two = [df_two.index[i] for i in matched_indices[1]]
#        matched_pairs = OrderedDict(
#                pairs=zip(tuple(zip([df_one.index[i] for i in matched_indices[0]],
#                                        [df_two.index[i] for i in matched_indices[1]]))),
#                deltamz=list(),
#                deltart=
                                    
                              
        


        number_of_matches = len(matched_indices[0])
        return number_of_matches, swap
                    
        
#        # do RT match first    
#        rt_subtraction_matrix = np.abs(np.array([df_one['rt']]).T - np.array([df_two['rt']]))
#        rt_midpoint = np.mean(rt_range)
#        rt_spread = abs(rt_range[0]-rt_midpoint)        
#        rt_subtracted_boolean = abs(rt_subtraction_matrix) - rt_midpoint <= rt_spread        
#        rt_matched_indices = np.where(rt_subtracted_boolean)
#        # np.where produces a tuple of (array_of_x_indices, array_of_y_indices) 
#        # where the match occurred.
#        
#        
#        if len(rt_matched_indices[0]) <= 0:
#            return 0, False
#        
#        # now do the mz
#        
#        mz_subtraction_matrix = np.abs(np.array([df_one['mz']]).T - np.array([df_two['mz']]))
#        mz_matched_boolean = np.isclose(additive, mz_subtraction_matrix, 
#                                        atol=tolerance, rtol=0)
        
        # then, use np.sum to count the number of "AND" matches
#        return np.sum(mz_matched_boolean & rt_subtracted_boolean), swap    

    def execute(self):

#        pdb.set_trace()
        print('starting the NeuAc match')
        # if there are no graphs AND graphmls loaded, prompt for graphml files        

        mz_matrix, file_list, filedir, f = self.check_input_graphs()

        # if a folder has not been supplied, then prompt for choice.
        if self.outputfolder == '':
            folder = custom_tools.getfolder('Choose output folder')
        else:
            # if an output folder WAS specified
            folder = self.outputfolder
        
        
        unique_pairs = creatematrix(len(mz_matrix.iloc[0]))        
        matched_pairs = pd.DataFrame()
        before=0
        func_calc_complete = lambda x : int((x/len(unique_pairs)*100))
        
        # function for threading the status update to speed up the program.
        def update_status(completedpairs, previousvalue):
            print('Percent complete = ' + str(completedpairs) + '%') if (completedpairs % 5 == 0) and (completedpairs!=previousvalue) else False
        
            

        # this cycles through each of the unique pairs. The main loop!        
        for (ind, (s, t)) in enumerate(unique_pairs):
## I should think about vectorising this! make below all a functon and 
# vectorise map it onto the unique_pairs.
            # display a sort of progress indicator
#            complete = int((ind/len(unique_pairs)*100))
#            complete = func_calc_complete(ind)
#            print('Percent complete = ' + str(complete) + '%') if (complete % 5 == 0) and (complete!=before) else False
#            print('comparing subgraphs: {} and {}'.format(s, t))
            complete = func_calc_complete(ind)
            Thread(target=update_status, args=(complete, before)).start()
            before=complete

            
            for k, values in enumerate(self.df_cluster.itertuples()):
            

    ### COMPARESUBGRAPHS CALLED HERE
#                pdb.set_trace()
                no_of_matches, swap = \
                    self.comparesubgraphs(self.graphlist[s], 
                                          self.graphlist[t], 
                                          rt_range=[self.df_cluster.loc[k, 'cluster_min_rt'], 
                                                    self.df_cluster.loc[k, 'max_rt']
                                                    ]
                                          )
                
#                if swap: # reverse the order of subgraphs to make s always larger and later.
#                    import copy
#                    one = copy.deepcopy(s)
#                    two = copy.deepcopy(t)
#                    s = two
#                    t = one
    
                
                # this defines the first element of matches 
                # (ie rows in  what will become matched_pairs)
                matches = pd.DataFrame([[(s,t),
                                        no_of_matches,
                                        ]])
                
                # count the total nonzeros in the original lists
                # count of nonzeros = number of nodes in that Graph
                # because zeros occur due to padding the matrix into a
                # rectangle.
                total = pd.DataFrame(dict(num_1=[mz_matrix.iloc[:,s].count()],
                                          num_2=[mz_matrix.iloc[:,t].count()],
                                          ),                         
                                     )
                # if the number of matches divided by the greater of the 
                # totals meets the threshold, then continue with matching.
                # else, try matching to the next additive, and so on.                
                if (no_of_matches/float(np.maximum(total.loc[:, 'num_1'], 
                                         total.loc[:, 'num_2'])) <= self.threshold):
                    continue
                    

            matches = pd.concat([matches, total], axis=1, ignore_index=False)
            # matches is NOW the 1-row dataframe with:
            # (s,t); comparesubgraphs(); (totalmatches1, totalmatches2)
            
            
            # append the additive mass
            matches.loc[0,'additive']=float(self.df_cluster.loc[k, 'difference'])
            
            
                        # update the growing dataframe of matches
            # matched_pairs is the master list, matches are the single rows.
            matched_pairs = pd.concat([matched_pairs, matches],
                                      ignore_index=True)
            



        # reference the original subgraphs
        matched_pairs['subgraphs'] = matched_pairs.iloc[:,0].apply(lambda x :
                                                                   (os.path.splitext(
                                                                       os.path.basename(
                                                                           file_list[x[0]]))[0],
                                                                    os.path.splitext(
                                                                        os.path.basename(
                                                                            file_list[x[1]]))[0],
                                                                    ))

        # try to guess at the true subgraph number using regex
        matched_pairs['subgraphs_guess']=matched_pairs['subgraphs'].apply(
            lambda x: (int(re.search('(?<=subgraphs)(\d+)',
                                 x[0]).group(0)),
                       int(re.search('(?<=subgraphs)(\d+)',
                                 x[1]).group(0)),
                       )
            if isinstance(x, tuple) else ('?','?'))


        # create and rearrange the columns
        matched_pairs['pairings']=matched_pairs.iloc[:,0]
        matched_pairs['matches']=matched_pairs.iloc[:,1]
        matched_pairs['percent1']=matched_pairs.loc[:,'matches']/matched_pairs.loc[:,'num_1']
        matched_pairs['percent2']=matched_pairs.loc[:,'matches']/matched_pairs.loc[:,'num_2']
        matched_pairs['percent_for_thresholding']=np.maximum(matched_pairs['percent1'], matched_pairs['percent2'])
        

        ##print('matched_pairs = \n', matched_pairs)


        # make a note of the source of the file, so
        # that I can track the method i used.
        matched_pairs = pd.concat([matched_pairs,
                                   pd.DataFrame(dict(subgraphs=f,
                                                     matches=1), index=[0])
                                   ],
                                  ignore_index=True)




        # final rearrangements
        matched_pairs = matched_pairs.loc[:,['pairings',
                                             'subgraphs',
                                             'subgraphs_guess',
                                             'matches',
                                             'num_1',
                                             'num_2',
                                             'percent1',
                                             'percent2',
                                             'percent_for_thresholding',
                                             'additive',
                                             ]
                                          ]

        # remove the noninformative zeroes    
        trimmed = matched_pairs.loc[matched_pairs.loc[:, 'matches'] > 0, :]
        #print('trimmed matched_pairs = \n', trimmed)

        
            
        if folder !='':
            writer  = pd.ExcelWriter(folder + '\\NeuAc_linking_output.xlsx')
            trimmed.to_excel(writer, 'positive_hit_list')
            matched_pairs.to_excel(writer, 'full_list_of_matched_pairs')            
            writer.save()
            
        else:
            # this line will fail if called from a different module,
            # with graphs specified BUT no folder specified.
            try:
                folder = os.path.join(os.path.dirname(self.get_graphmlstolink()[0]),
                                  'NeuAc linking')
            except:
                # in the worst case scenario, folder will default to
                # somewhere...at least the data won't be lost!
#                https://stackoverflow.com/questions/273192/how-can-i-create-a-directory-if-it-does-not-exist/14364249#14364249
                print('error: output folder not specified/invalid, defaulting to NeuAc linking folder somewhere on your pc')
                import distutils.dir_util
                distutils.dir_util.mkpath('\\NeuAc linking')
                folder='\\NeuAc linking'
        
        

 
        # Take the lowest of the two percentages and use that for
        # the thresholding. Because the largest graph will always
        # have the lower percentage, and I assume that the 
        # cluster with the plus-additives are always smaller in number
        # than the other cluster (without the additive).        
        to_join = trimmed.loc[trimmed.loc[:,'percent_for_thresholding'] >= self.threshold, :]
        
   
        if len(to_join) == 0:
            print('no matches were found. ending neuac matching.')
            return False
    

        # for each match in the pairings column, merge those subgraphs
        # use a graph with edges made from the pairings!
        # ie use a new graph with nodes=subgraph number and edges = groupings.
        # note this is a graph of graphs, not editing the subgraphs themselves
        H = nx.Graph()
        # these add nodes and edges from a list
        H.add_nodes_from([os.path.splitext(os.path.basename(x))[0] for x in file_list])
        
        # concatenate the tuples so as to add "additive" mass data onto the edge
        # by putting the second term in brackets with a comma, i hope to be able to simply add them.
#        pdb.set_trace() # because this next line probably will crash
#        H.add_edges_from(to_join.loc[:,'subgraphs'] + (dict(additive_mass=to_join.loc[:,'additive']),))
        composed_edges = self.df_into_edges_with_attr(to_join.loc[:,'subgraphs'], to_join.loc[:,'additive'], 'additive')
        H.add_edges_from(composed_edges)
        # add_edges_from takes in (u,v, d) where d is a dictionary of attributes.

        # this extracts all the unconnected subgaphs in H, the graph of 
        # graphs that should be joined
        graphs = sorted(nx.connected_component_subgraphs(H, copy=True),key=len, reverse=True)
        
        # now for each extracted subgraph, I simply lookup the nodes,
        # and combine those subgraphs indicated by those nodes.        
        # for each node within the subgraph:
        for i, subgraph in enumerate(graphs):
            # define an empty graph to put the combined subgraphs
            # here is where I actually combine the subgraphs
            L_combine = nx.Graph()
            

            num_list = []
            nodeappend = num_list.append
            # this cycles through each node in the sub subgraph of "graphs":
            for nodestr in subgraph:
                #print('nodestr = ', nodestr)
                # get the graph number
                num = int(re.search('(?<=subgraphs)(\d+)', nodestr).group(0))
                nodeappend(num) # grows the list for the final graphml name to show
                
                # this reads the original graphml from the folder,
                ######### but i should make it read from the graphml list instead.
                ##### here is the problem, because it doesn't read the file correctly!
                graph_path=os.path.join(filedir, nodestr + '.graphml')
                #print('graph_path=', graph_path)
                L = nx.read_graphml(path=graph_path)
                
                # appends the L_combinewith the new graph
                L_combine = nx.compose(L, L_combine) 
            
            if len(subgraph)>1:
                L_combine = self.findneuac(L_combine)                  

                
               
            graphname = os.path.join(folder, str(i) + 'neuacmerged' + str(num_list) + '.graphml')
            
            # update class properties
            self.outgraphlist.append(graphname)    
            self.outgraphs.append(L_combine)
            
            ### write to graphml
            nx.write_graphml(L_combine, path=graphname)
        
        print('Opening save directory')
        custom_tools.opendir(folder)
        
        #print('self.outgraphlist=\n', self.outgraphlist)
        #print('self.outgraphs=\n', self.outgraphs)
        print('Neuac linking Finished')
        return True
    
    def findneuac(self, graphin):
        # this takes concatenated subgraphs (ie two graphs combined into one),
        # finds at least one neuac and creates an edge in the graph
        # then returns the updated graph

        df_nodes = nodes_to_dataframe(graphin, ['mz','rt'])

        
        # make a persistent list of edges to be added all at once later
        # in the format of [(u,v, dict), (), ()]
        persistent_edge_list = []   
        persappend = persistent_edge_list.append
        
        for current_iterated_node in df_nodes.itertuples():
            # current_iterated_node[0] is the node index, current_iterated_node[1] is the mz


            for i, row in enumerate(self.df_cluster.itertuples()):
                jumpmass = float(self.df_cluster.iloc[i, self.df_cluster.columns.get_loc('difference')])
                
                tomatch = current_iterated_node[1] + jumpmass # 291.0954
#                pdb.set_trace()
                match_bool = np.isclose(tomatch, df_nodes.loc[:,'mz'], atol=0.02, rtol=0) #& \
                               # ((df_nodes.loc[:,'rt']-current_iterated_node[2] >= self.rt_range[0]) & ((df_nodes.loc[:,'rt']-current_iterated_node[2] <= self.rt_range[1])))
                if True in match_bool:
                    # restrict for RT
                     
                    
                    # there should only be one true match so this should be a very short run
                    for matched in df_nodes.loc[match_bool].itertuples():
                        # this should iterate through a dataframe with any matches
                        # matched[0] is index, [1] is mz, [2] is RT
                        
                        # check if within RT limits                        
                        
                        # check if there are any one node paths between them = skip
                        if nx.has_path(graphin, current_iterated_node[0], matched[0]):
                            if len(nx.shortest_path(graphin, source=current_iterated_node[0],
                                             target=matched[0])) <= 2:
                                continue
                            
                        # collect the data to go into data dictionary.
                        data_dict = {'delta_m/z':float(abs(current_iterated_node[1]-matched[1])),
                                     'delta_RT':float(current_iterated_node[2]-matched[2]), 
                                }
                                                
                        for col_name in self.glymod_full_colnames_list:
                            data_dict[col_name]=int(self.df_cluster.iloc[i, self.df_cluster.columns.get_loc(col_name)])

                        # append the list of edges, to be drawn all at once later.
                        persappend((int(current_iterated_node[0]), 
                                                     int(matched[0]),data_dict,)
                                                     )
                                


        graphin.add_edges_from(persistent_edge_list)
        return graphin
        
        
    def df_into_edges_with_attr(self, df_of_tuples, df_of_attributes, name_of_attribute):

        
        vector_of_dicts = (df_of_attributes.apply(lambda x : ({name_of_attribute:x},)).values)
        vector_of_tuples = (df_of_tuples.values)
        
        return vector_of_tuples + vector_of_dicts  
        

    def check_input_graphs(self):
        if self.get_inputgraphs()==[]:
            if self.get_graphmlstolink()==[]:
                self.set_graphmlstolink(custom_tools.getfiles('Select the graphml files to link', '.graphml'))
                
            # mz_matrix stores each Graph as a column, with the rows being
            # the masses of each node in that Graph.
            mz_matrix = pd.DataFrame()
            file_list = self.get_graphmlstolink()
            filedir = os.path.dirname(file_list[0])
            graphlist = [] # list of file paths to graphmls
            graphappend = graphlist.append
            for i, f in enumerate(file_list):
                G = nx.read_graphml(path=f, node_type=int) #loads the next graphml file into graph G.
                mz_matrix = pd.concat([mz_matrix,
                                      pd.DataFrame(list(nx.get_node_attributes(G,'mz').values()))],
                                      axis=1,
                                      ignore_index=True,
                                      )
                graphappend(G)
        # if there are graphs already loaded, simply read them instead of prompting for graphmls
        else:
            mz_matrix = pd.DataFrame()
            for i, f in enumerate(self.get_inputgraphs()):
                mz_matrix = pd.concat([mz_matrix,
                                       pd.DataFrame(list(nx.get_node_attributes(f,'mz').values()))],
                                      axis=1,
                                      ignore_index=True,
                                      )
        
        self.graphlist = graphlist

        return (mz_matrix, file_list, filedir, f)
        
        


print('importing linksubgraphs.py\nmain class=linksub(graphmlstolink = [], graphs=[])\n',      
      'properties: get_outgraphlist, get_inputgraphs, get_graphmlstolink, get_outgraphs\n',
      'methods: execute, set_inputgraphs, set_graphmlstolink\n',
      )

"""
def __init__(self, graphmls = [], graphs=[], 
                 outputfolder='', threshold=0.6,
                 df_cluster=None)
"""

if __name__ == "__main__":
    b = linksub(threshold=0.4)
    
    