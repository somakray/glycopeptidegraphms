# -*- coding: utf-8 -*-
"""
Created on Tue Dec  5 15:53:26 2017

@author: choom

the aim of this script is to take:
    (1) a cleaned consensusxml.csv of the data
    (2) the byonic psms search of the same or equivalent data, exported as a csv

Process:
    (3) pre-process the byonic data for headers
    (4) match byonic IDs to consensusxml nodes, using all the scripts I have for align byonic
    (5) sum the abundances and calculate relative abundances, dividing by either:
        (a) total pep + gpep
        (b) pep
        (c) gpep
    (6) export stats as excel file
    
Extras:
    (21) convert consensusxml nodes to a graph, with all the attributes
    (22) export as three graphmls: pep, gpep, unidentified
    (23) do a plot and color the peptides one color, the glycopeptides another color, and everything else in gray
    

"""

from graphms import custom_tools, glycanformat
import pandas as pd, numpy as np
import os
import pdb
from threading import Thread

def isjupyternotebook():
    try:
        from IPython import get_ipython
        return 'ipykernel.zmqshell.ZMQInteractiveShell' in str(get_ipython())
    except:
        return False

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
    return False

#def mzmatchlist(mz, rt, ppmErr=15):
#    # incoming r is a numpy row slice
#    # mz = float(r['mz_cf']) # this is the data MZ
#    # rt = float(r['rt_cf']) # this is the data RT
#    mz = float(mz)
#    rt = float(rt)
#    maxval = (1000000 / (1000000 - ppmErr)) * mz
#    minval = mz - (maxval - mz)
#    mask1 = np.array(byonicid['mz_in_byonic'] <= maxval)
#    mask2 = np.array(byonicid['mz_in_byonic'] >= minval)
#    mask3 = np.array(byonicid['rt_in_byonic'] <= (rt + rt_tolerance))
#    mask4 = np.array(byonicid['rt_in_byonic'] >= (rt - rt_tolerance))
#    mainmask = mask1 & mask2 & mask3 & mask4
#    byonicid['hasmatch'] = byonicid['hasmatch'] | mainmask
#    byonicid['hasmzmatch'] = byonicid['hasmzmatch'] | (mask1 & mask2)
#
#    if True in mainmask:
#        # log_now('mainmask = \n', mainmask)
#        # get the indices of where the matches are
#        matchindex = (np.where(mainmask == True)[0])
#        ## get the values of matches
#        # this just cuts out the whole row
#        matchrow = byonicid.ix[matchindex, :]  
#
#        # this gets an indexed arrray of matches >= 1.
#        matchvalues = pd.DataFrame(columns=cols)
#        # by doing a concatenate with the empty matchvalues dataframe, I simply catch all the values of byonic
#        matchvalues = pd.concat([matchvalues, matchrow])
#
#        # now, I have to isolate the BEST RT fit, i.e. minimise the difference
#        # from the indexed dataframe of matchvaluesrt, subtract the dataRT, absolutise, get minimum
#        matchvalues['rtdifference'] = np.abs(matchvalues['rt_in_byonic'] - rt)
#        matchvalues.sort_values('rtdifference', axis=0, ascending=True, inplace=True)
#        matchvalues.reset_index(drop=True, inplace=True)
#        log_now(logging.DEBUG, 'match values before  isolation = \n', matchvalues)
#        # = ([matchvalues.iloc[0,0]], [matchvalues.iloc[0,1]], [matchvalues.iloc[0,2]]) # comes out as a series with 3 items
#        # gets the best match of byonic to the data point based on the RT sort
#        matchvalues = matchvalues.loc[[0]]
#        log_now(logging.DEBUG, 'match values after isolation = \n', matchvalues)
#        # isolate the minimum
#        return matchvalues
#    else:
#        return pd.DataFrame([['.'] * len(cols)], columns=cols)

class alignbyonictocsv:
    
    def __init__(self, csvfile='', byonicfile='', savefile='', dojupytercheck=False, rtcorrection=0.,
                 mz_tol_ppm=15, rt_tol=60):
        print('inputs are:\n{}\n{}\n{}\n{}'.format(csvfile, byonicfile,savefile,dojupytercheck))
        # get and store the file paths
        if csvfile=='': csvfile = custom_tools.getfile('Select cleaned consensusxml csv file', '.csv')
        if byonicfile=='': byonicfile = custom_tools.getfile('Select Byonic ouputted PSM file', '.csv') 
        self.dojupytercheck = dojupytercheck
        if isjupyternotebook() and not self.dojupytercheck:
            try:
                csvfile.seek(0)
                byonicfile.seek(0)
            except:
                pass
            
        self.csvfile = csvfile
        self.byonicfile = byonicfile
        self.savefile = savefile 
        self.rtcorrection = rtcorrection
        self.mz_tol_ppm = mz_tol_ppm
        self.rt_tol = rt_tol
        
        self.execute()

        custom_tools.openfile(self.savefile)
        pass
    
    def execute(self):
        byonicid = importandfixbyoniccsv(self.byonicfile)
        df = importandfixconsensuscsv(self.csvfile)
        
        # run the matching algorithm
        matchdf = self.domatch(df, byonicid,self.mz_tol_ppm,self.rt_tol)
        # export the files as excel sheets
        
        self.serversaveexcelfile(matchdf)
        print('Alignment of Byonic PSMs to consensusxml features Finished')
        
   
    def serversaveexcelfile(self, filetosave):
        
        if isjupyternotebook() and self.dojupytercheck:
            #import IPython.display as ip
            import os
            
            dirstring = './serveroutput/alignbyonictocsv/'
            
            if not os.path.exists(dirstring):
                os.makedirs(dirstring)            
            
            self.exportexcel(filetosave, dirstring + 'data.xlsx')
            self.downloadpath = dirstring
            #ip.FileLinks(dirstring)
            
            
            
        else:
            if self.savefile=='': self.savefile=custom_tools.getsavefile('Name the excel file', '.xlsx')
            self.exportexcel(filetosave, savefilename=self.savefile)

    
    def domatch(self, csvdf, byonicid, ppmErr=15, rttolerance=60):

        # create a copy of the csvdf for the matches to go into
        matchdf = csvdf.copy(deep=True)
        finalcopydf = pd.DataFrame()
        ticker = 0

        for row, rt, mz in csvdf.loc[:, ['rt_cf', 'mz_cf']].itertuples():
            row=int(row)
            rt=float(rt)-self.rtcorrection
            mz=float(mz)
#            maxval = (1000000 / (1000000 - ppmErr)) * mz
#            minval = mz - (maxval - mz)            
#            mask1 = np.array(byonicid['mz_in_byonic'] <= maxval)
#            mask2 = np.array(byonicid['mz_in_byonic'] >= minval)
#            mask3 = np.array(byonicid['rt_in_byonic'] <= (rt + rttolerance))
#            mask4 = np.array(byonicid['rt_in_byonic'] >= (rt - rttolerance))            
#            mainmask = mask1 & mask2 & mask3 & mask4
#            
#            byonicid['hasmatch'] = byonicid['hasmatch'] | mainmask
#            byonicid['hasmzmatch'] = byonicid['hasmzmatch'] | (mask1 & mask2)
            
            # use numpy instead to match
            rtmatches = np.isclose(rt, byonicid.loc[:,'rt_in_byonic'], atol=rttolerance, rtol=0.)
            if sum(rtmatches)==0: continue
            mzmatches = np.isclose(mz, byonicid.loc[rtmatches,'mz_in_byonic'], atol=0, rtol=ppmErr*1e-6)
            if sum(mzmatches)==0: continue
            
            ## otherwise, if there IS a match, take the closest in RT
#            pdb.set_trace()
#            print('match found at row = ', row)           
            t = Thread(target=lambda x : print('match found at row = {}\n'.format(x)), args=[str(row)])
            t.start()
            t.join()
            
            if sum(mzmatches)==1:
                copydf = byonicid.loc[rtmatches,:].loc[mzmatches,:]
                copydf.index = [row] # force the index so i can concat later                
            else:
                allmatches = (byonicid.loc[rtmatches,:]).loc[mzmatches,:]
                allmatches['rtdifference'] = np.abs(allmatches['rt_in_byonic'] - rt)
                allmatches.sort_values('rtdifference', axis=0, ascending=True, inplace=True)
                allmatches.reset_index(drop=True, inplace=True)
                allmatches = allmatches.iloc[[0],:]
                allmatches.index = [row]
                copydf = allmatches
                
                pass
            
            finalcopydf = pd.concat([finalcopydf, copydf])
#            matchdf = pd.concat([matchdf, copydf], axis=0)

#            print(matchdf.loc[matchdf['mz_in_byonic']>0,:])
            ticker+=1
#            if ticker>3: break
#        pdb.set_trace()  
        matchdf=pd.concat([matchdf, finalcopydf], axis=1)
        return matchdf
#        print(matchdf)
#        if savefile=='': savefile= custom_tools.getsavefile('select csv','.csv')
#        matchdf.to_csv(savefile)
#        custom_tools.openfile(savefile)
        

    
    
    def exportexcel(self, df, savefilename):
        writer = pd.ExcelWriter(savefilename)
        df.loc[df.loc[:,'rt_in_byonic']>0,:].to_excel(writer, 'out1_matches', index_label='node')       
        print('excel file exported to: {}'.format(savefilename))
          

def importandfixbyoniccsv(f):
    if isinstance(f, pd.DataFrame):
        bdf = f
    elif isinstance(f, str):
        bdf = pd.read_csv(f, header=0, dtype={'z':np.int}, na_filter=True, na_values='.', index_col=False)
    else:
        print('incorrect file parsed: ', f)

    bdf.reset_index(drop=True, inplace=True)
    bdf = custom_tools.checkcolumnsandreplace(bdf)
    #extract from this the rt, mz, ID
    byonicid = pd.DataFrame() #initialises as dataframe
    # byonicid['Obs.\nMH'] = bdf['Obs.\nMH']
    # this appends the MH value into the new dataframe.
    # byonicid = pd.concat([byonicid, bdf['Obs.\nMH'].apply(pd.to_numeric, errors='coerce')-1.0078], axis=1)
    #print("hi")
    #print(list(bdf['Mods\n(variable)'].fillna(value='.')))
    byonicid['mz_in_byonic'] = bdf['Obs.\nMH'].apply(pd.to_numeric, errors='coerce')-1.0078
    byonicid['rt_in_byonic'] = bdf['Scan\nTime'].apply(pd.to_numeric, errors='coerce')*60.0
    byonicid['precursor_mz'] = bdf['Obs.\nm/z'].apply(pd.to_numeric, errors='coerce')
    byonicid['precursor_charge'] = bdf['z'].apply(pd.to_numeric, errors='coerce').apply(int)
    byonicid['ppm_error'] = bdf['ppm\nerr.'].apply(pd.to_numeric, errors='coerce')
    byonicid['scan_no'] = bdf['Scan #'].apply(lambda v: int(str(v).split('=')[-1]))
    byonicid['peptide'] = bdf['SequenceOriginalDebug']
    byonicid['pep2D'] = bdf['PEP\n2D'].apply(pd.to_numeric, errors='coerce')
    byonicid['score'] = bdf['Score'].apply(pd.to_numeric, errors='coerce')
    byonicid['glycan'] = bdf['Glycans'].apply(lambda x: '.' if x != x else x)
    byonicid['glycopep'] = byonicid['peptide'] + ' @\n' + byonicid['glycan'].apply(lambda x: glycanformat.glycan_convert_format(x,3))
    byonicid['hasmatch'] = [False] * len(byonicid['peptide'])
    byonicid['hasmzmatch'] = [False] * len(byonicid['peptide']) 
    byonicid['Mods(variable)'] = bdf['Mods\n(variable)']
    byonicid['glycanclass'] = bdf['Mods\n(variable)'].fillna(value='.').apply(lambda x : noroglycan(x))
    try: # just in case it was exported without all columns shown
        byonicid['protein'] = bdf['Protein\nName']
    except:
        byonicid['protein'] = bdf['SequenceOriginalDebug']
    # log_now('byonicid = \n', byonicid)
    
#    # columns for use in the output dataframe and csv
#    self.cols = ['rtdifference'] + list(byonicid.columns)
    
    return byonicid

def noroglycan(t):
    if ('NGlycan' in t) and ('OGlycan' in t):
        return 'both'
    elif 'NGlycan' in t:
        return 'N'
    elif 'OGlycan' in t:
        return 'O'
    else:
        return '.'

def importandfixconsensuscsv(f):
    df = pd.read_csv(f, index_col=False, header=0, sep=',', na_filter=False,)  # opens the file with everything intact    
    df = df.iloc[:,:].apply(pd.to_numeric, errors='coerce')
    df.reset_index(drop=True, inplace=True)
    df.index+=1
    df = custom_tools.checkcolumnsandreplace(df)
    df = filterintensity(df, 1e6)
    return df
    pass

def filterintensity(df, intensity):
    return df.loc[df['intensity_cf']>= intensity, :]

def batchfilelist(fileoffilelist=''):
    if fileoffilelist=='': fileoffilelist = custom_tools.getfile('Please choose the file containing the list of pairs of files to process', '.csv')
    output_file_list = []
    batchdf = pd.read_csv(fileoffilelist, index_col=0)
    for index, a, b, c in batchdf.itertuples():
        z = alignbyonictocsv(a,b,c)
        output_file_list.append(c)
    print('done')
    return output_file_list

def makebatchfilelist():
    x = custom_tools.getfiles('select list of input KNIME consensusxml csv files', '.csv')
    y = custom_tools.getfiles('select list of input Byonic PSMs files', '.csv')
    z = custom_tools.getfolder('Select output folder for the processed files')
    df = pd.DataFrame()
#    pdb.set_trace()
    for i in range(0,max(len(x),len(y))):
        try:
            df.loc[i, 0] = x[i]
        except:
            pass
        try:
            df.loc[i, 1] = y[i]
        except:
            pass
        try:
            df.loc[i,2] = os.path.join(z, os.path.splitext(os.path.basename(y[i]))[0] + '.xlsx')
        except:
            pass
    sf = custom_tools.getsavefile('Please name the file of list of files', '.csv')
    df.to_csv(sf, index_label="index")
    custom_tools.openfile(sf)
    return sf


def calcrelativeabundances(filelist):
    filelist = custom_tools.getfiles('Choose the xlsx files with the aligned byonic files', '.xlsx') if len(filelist)<1 else filelist
    savefile=custom_tools.getsavefile('Name your excel output file', '.xlsx')
    writer  = pd.ExcelWriter(savefile)
#    df.loc[df.loc[:,'rt_in_byonic']>0,:].to_excel(writer, 'out1_matches', index_label='node')       
    statdf = pd.DataFrame()
    
    for index, f in enumerate(filelist):
#        pdb.set_trace()
        df = pd.read_excel(f, sheetname=0, index_col=0)
        peptideabundance = sum(df.loc[df.loc[:,'glycan']=='.', 'intensity_cf'])
        gpepabundance = sum(df.loc[df.loc[:,'glycan']!='.', 'intensity_cf'])
        totalabundance = peptideabundance + gpepabundance
        ngpepabundance = sum(df.loc[df.loc[:,'glycanclass']=='N', 'intensity_cf'])
        ogpepabundance = sum(df.loc[df.loc[:,'glycanclass']=='O', 'intensity_cf'])
        
        df['peptideabundance'] = [peptideabundance] * len(df.index)
        df['glycopeptideabundance'] = [gpepabundance] * len(df.index)
        df['relpepabundance'] = df['intensity_cf'] / peptideabundance
        df['relglycopepabundance'] = df['intensity_cf'] / gpepabundance
        df['reltotalabundance'] = df['intensity_cf'] / totalabundance
        df['relngpep'] = df['intensity_cf'] / ngpepabundance
        df['relogpep'] = df['intensity_cf'] / ogpepabundance
        
        statdf= statdf.append(pd.DataFrame([[f,
                                     peptideabundance, 
                                     gpepabundance, 
                                     totalabundance,
                                     ngpepabundance,
                                     ogpepabundance,
                                     peptideabundance/totalabundance,
                                     gpepabundance/totalabundance, 
                                     ngpepabundance/totalabundance,
                                     ogpepabundance/totalabundance,   
                                     ngpepabundance/(peptideabundance+ngpepabundance)
                                            ]],
                        columns=['file name',
                                 'peptideabundance', 
                                 'gpepabundance',
                                 'totalabundance',
                                 'ngly abundance',
                                 'ogly abundance',
                                 'peptide/total',
                                 'gpep/total',
                                 'ngly/total',
                                 'ogly/total',
                                 'ngly/(peptide+ngly)',
                                 ],                                                                   
                                 ))
        
        df.to_excel(writer, sheet_name=str(index)+(os.path.splitext(os.path.basename(f))[0])[-20:], index_label='node')
        
    statdf.reset_index(drop=True, inplace=True)
    statdf.to_excel(writer, sheet_name='stats', index_label='index')
    writer.save()
    custom_tools.openfile(savefile)
    return savefile

def calculateuniqueglycopeptides():
    filelist = custom_tools.getfiles('Choose the csv files with the byonic psm output', '.csv')
    savefile=custom_tools.getsavefile('Name your excel output file', '.xlsx')
    writer  = pd.ExcelWriter(savefile)
#    df.loc[df.loc[:,'rt_in_byonic']>0,:].to_excel(writer, 'out1_matches', index_label='node')       
    statdf = pd.DataFrame()
    
    for index, f in enumerate(filelist):
        df = importandfixbyoniccsv(f)
        unique_peps = df['glycopep'][~df[['glycopep']].duplicated()] 
        
        mask1 = pd.Series(~df['glycopep'].duplicated()) & pd.Series(df['glycanclass']=='N')
        unique_n = df[mask1]
#        unique_peps.apply(lambda x : x[-1:])=='.'
#        pdb.set_trace()    
        
        numunique = len(unique_peps.index)
        numpeps = len(unique_peps[unique_peps.apply(lambda x : x[-1:])=='.'])
        numgpeps = len(unique_peps[unique_peps.apply(lambda x : x[-1:])!='.'])
        numngpeps = len(unique_n)
        
        statdf = statdf.append(pd.DataFrame([[f, numunique,
                                              numpeps,
                                              numgpeps,
                                              numngpeps,
                                              ]],
                                            columns=['file name', 
                                                     'unique peptides plus glycopeptides',
                                                     'unique peptides',
                                                     'unique glycopeptides',
                                                     'unique N-glycopeptides',
                                                     ],
                                            ))
    
    statdf.reset_index(drop=True, inplace=True)    
    statdf.to_excel(writer, sheet_name='stats', index_label='index')
    writer.save()
    custom_tools.openfile(savefile)    
    
print('Imported - alignbyonictocsv.py version 2.1')

def execute():
    startindex = 2
    savedir = custom_tools.getfolder('select the directory to save output aligned file')

    for index, (consensus, byonic) in enumerate(
            zip(custom_tools.getfiles('Select list of cleaned consensus csv files', '.csv'),
                custom_tools.getfiles('Select list of byonic psms csv files', '.csv')
                )):
        alignbyonictocsv(csvfile=consensus, byonicfile=byonic,
                         savefile=os.path.join(savedir, ''.join(
                             [str(index + startindex), '_', os.path.basename(byonic), '.xlsx'])),
                         rtcorrection=0,
                         rt_tol=60
                         )

def execute_on_single_pair_of_files_with_defined_inputs_return_result_dict(savedir, consensus_filepath,
                                byonic_psm_filepath, save_excel_file_name, rtcorrection=0, rt_tol_in_seconds=60):

    result = alignbyonictocsv(csvfile=consensus_filepath, byonicfile=byonic_psm_filepath,
                         savefile=save_excel_file_name,
                         rtcorrection=rtcorrection,
                         rt_tol=rt_tol_in_seconds)

    return dict(alignbyonictocsv_object=result,
                saved_aligned_excel_file_name=save_excel_file_name)

if __name__ == "__main__":
#    from collections import namedtuple
#    batchlist= namedtuple('batchfilepairs', 'consensuslist psmlist',)
    
#    batchlist = batchlist(consensuslist=custom_tools.getfiles('Select list of cleaned consensus csv files', '.csv'),
#                          psmlist=custom_tools.getfiles('Select list of byonic psms csv files', '.csv')
#                          )
    startindex=2
    savedir = custom_tools.getfolder('select the directory to save output aligned file')
    
    for index, (consensus, byonic) in enumerate(zip(custom_tools.getfiles('Select list of cleaned consensus csv files', '.csv'),
                    custom_tools.getfiles('Select list of byonic psms csv files', '.csv') 
                    )):
        alignbyonictocsv(csvfile=consensus, byonicfile=byonic,
                         savefile=os.path.join(savedir, ''.join([str(index+startindex),'_',os.path.basename(byonic),'.xlsx'])),
                         rtcorrection=0,
                         rt_tol=60
                         )
    
#    a = r'O:\Analytics\Matt\035 HILIC Paper\Final analysis for AOMSC, Dec 2017\01 hilic csv\Herceptin50ug_nonenrich_iHILIC_2ugul_5ug_C18_OT_MS1_mz500_2000_60min_BA2_3_knime__corrected.csv'
#    b = r'O:\Analytics\Matt\035 HILIC Paper\Final analysis for AOMSC, Dec 2017\02 byonic psms\herceptin\01a herceptin nonenr BA2_1 psm.csv'
#    z = alignbyonictocsv(a,b)
#    makebatchfilelist()
#    batchfilelist()
#    calcrelativeabundances()


