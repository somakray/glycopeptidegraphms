"""
applyindirectcompositions v1-4 on 160718

this program is to take the output of predictcompositions,
which is a list of relative compositions versus reference nodes.
and apply a manual list of the compositions of those ref nodes
to derive the predicted compositions of each node.
"""



import pandas as pd
import numpy as np
import os
from graphms import custom_tools, glycanformat


pd.set_option('mode.chained_assignment', None)

def df_columns_to_dict(t):
    # input a dataframe
    # get the column names
    # serialise the column names
    # output a dictionary that helps to convert the
    # column name into the column index
    # output a dicitonary that you can use thus:
    # >>> out_dict['column name']
    # >>> 0

    num = list(range(0,len(t.columns.values)))
    zipped = list(zip(t.columns.values, num))

    return {k:v for k, v in zipped}




def applyindirectcompositions(**kw):
    """

    :param kw: input your parameters as (relativecompfile=predcomp.csv, referencefile=refnodes.xlsx, savefile=abscompout.xlsx)
    :return: result = dict(resultpath=path to absolute composition file as *.xlsx,
                  resultdir=save directory as folder,
                  quantpath=path to quantitation file, as *_outPepQuant.xlsx,
                  )
    """
    #get the user to choose the files
#    export_csv = kw.get('export_csv') if kw.get('export_csv')!=None else False
    relativecomp_file = custom_tools.getfile(
            'Choose the file containing the predicted relative compositions', '.csv') \
            if kw.get('relativecompfile')==None else kw.get('relativecompfile')
    if len(relativecomp_file) < 1:
        print('Error no input file specified')
        return
    # abscomp_file refers to the reference nodes that have been filled in.
    abscomp_file = custom_tools.getfile(
            'Choose the XLSX file with the compositions of the reference nodes','.xlsx') \
            if kw.get('referencefile')==None else kw.get('referencefile')
    if len(abscomp_file) < 1:
        print('Error no input file specified')
        return

    # get the output of the save file
    savefile = os.path.splitext(custom_tools.getsavefile('Name the absolute_comps output file', '.xlsx'))[
                   0] + '.xlsx' \
                if kw.get('savefile')==None else kw.get('savefile')
    if len(savefile) < 1:
        print('Error no output file specified')
        return

    #load the files into a dataframe
    df_relcomp = pd.read_csv(relativecomp_file, header=0, na_filter=True, na_values='0')    
    df_relcomp.fillna(0,inplace=True)
    df_relcomp.set_index(['node'], drop=True, inplace=True)
    # set the columns to lowercase for homogeneity
    df_relcomp.columns = [x.lower() for x in df_relcomp.columns]
    
    # abscomp headers must be: node, mz_node, rt_node, glycans_or_mods*, pep
    df_abscomp = pd.read_excel(abscomp_file, sheetname=0, header=0, index_col='node', na_filter=True, na_values='0')
    df_abscomp.fillna(0,inplace=True)
    # set the columns to lowercase for homogeneity
    df_abscomp.columns = [x.lower() for x in df_abscomp.columns]
    ref_cols = df_abscomp.columns
    


    # collect the file paths into a df for reference
    df_info = pd.DataFrame([['relative compositions file path', relativecomp_file],
                            ['absolute compositions file',abscomp_file],
                            ['output file', savefile]])


    # initialise variables i will use
    ## get the list of glycans or modifications from the df_relcomp, which must be prefixed with sum_
    glycan = [x.split('sum_')[1] for x in df_relcomp.columns if 'sum_' in x] # 'NHFS'
    df_out = pd.DataFrame()
    df_out.index.names = ['node']
    # print(df_out)

#    col_dict = df_columns_to_dict(df_relcomp)

    ## first, use the given abscomp/refcomps to determine the compositions
    ## of the reference nodes
    # step 1, for each abscomp, find the corresponding subgraph and
    # hence find the corresponding smallest ref_node
    # combined_columns = combine_two_df_columns(df_relcomp, df_abscomp)
    true_ref_list = pd.DataFrame()

    for abs_counter, (abs_index, abs_row) in enumerate(df_abscomp.iterrows()):
        # this returns tuples of (index, named_tuples)
        # assume that the node indices are all wrong
        # so, match by mz and rt, thus:
        mz_node = float(abs_row['mz_node'])
        # this produces a boolean array for the match
        mz_match = np.isclose(mz_node, df_relcomp['mz_node'], atol=0, rtol=15e-06)

        #proceed to match rt
        rt_node = float(abs_row['rt_node'])
        # this produces a boolean array for the match
        rt_match = np.isclose(rt_node, df_relcomp['rt_node'], atol=60, rtol=0.)

        # check if a match even exists
        if True not in mz_match & rt_match:
            print('no match was found to user-supplied ref node:\n', abs_row)
            print('<<end of escape message>>')
            continue

        # find the intersection of mz_match and rt_match
        predmatch = df_relcomp.loc[mz_match & rt_match, :]
        # eventually, order the match by closeness to rt_node
        # and get the closest match
        predmatch.loc[:, 'rt_diff'] = abs(predmatch.loc[:,'rt_node'] - rt_node)
        predmatch = predmatch.sort_values('rt_diff', ascending=True).iloc[[0]]
        """

        at this point, predmatch is the row in df_relcomp that IS the refnode as specified in matched_ref_nodes.xlsx.
        I want to:
            1. specify in df_relcomp that this row is the reference row for that subgraph.
            2. point all the subgraph nodes to this node as the new refnode, in df_relcomp
            3. reset this row's sum_Xs as [0,0,0,0], and every other subgraph-node's sum_Xs relative to this.
        """

        ## 120718 - reset the ref node of the subgraph, and reclaculate the sum_Xs.
        # first, slice out the subgraph based on having the common refnode, taken from predmatch.
        df_subgraph_slice = df_relcomp.loc[df_relcomp.loc[:, 'refnode'] == int(predmatch.loc[:,'refnode']), :]

        # second, reset the refnode column to be the index of predmatch, populating for the length of the index.
        df_subgraph_slice.loc[:,'refnode'] =  [int(predmatch.index[0])] * len(df_subgraph_slice.index)
        # also reset the mz_refnode and mzdifference
        df_subgraph_slice.loc[:, 'mz_refnode'] = [float(predmatch.loc[:,'mz_node'])] * len(df_subgraph_slice.index)
        df_subgraph_slice.loc[:, 'mzdifference'] = df_subgraph_slice.loc[:, 'mz_node']-float(predmatch.loc[:,'mz_node'])
        # and reset the sum_Xs
        for g in glycan:
            sum_x_name = str('sum_' + g.lower())
            df_subgraph_slice.loc[:, sum_x_name] = df_subgraph_slice.loc[:, sum_x_name] - \
                                                   int(predmatch.loc[:, sum_x_name])

        # update df_relcomp with the newly updated df_subgraph_slice
        # .update will be "inplace", and you must not try to do x = x.update, which will return None.
        df_relcomp.update(df_subgraph_slice, overwrite=True)

        # 120718 - here, there is a need to set the index of df_abscomp, so I can index it later.
        # BUT! I cannot set it directly because I am actually iterating through it right now.
        # therefore, I need to create an external df and then replace it later.
        temp_df = df_abscomp.iloc[[abs_counter],:]
        temp_df.index = [int(predmatch.index[0])]

        true_ref_list = pd.concat([true_ref_list,
                                   temp_df], axis=0)

    # reset the df_abscomp to the value of the final temporary df.
    # because I couldn't modify df_abscomp while iterating through it.
    df_abscomp = true_ref_list



    ### loop through the rows of a relcomp file and apply the compositions.
    ### this is for the final output.
    for x, row in df_relcomp.iterrows():
        # slice the list by the index, i.e. node number
        row = df_relcomp.loc[[x]]

##        print('row = \n', row)       

        # get the refnode for the active row
        refnode = int(row['refnode'])

        # use this refnode to lookup the abscomp file for the direct match (via index), which was set earlier
        # from true_ref_list.
        
        try:
            abscomp_row = df_abscomp.loc[[refnode]]
##            print('abscomp_row = ', abscomp_row)
        except:
            # this advances the loop when the row is inaccessible
            # i.e. row doesn't exist.
            continue        

        # re-initialise the string for the concatenated composition
        comps = ''
        
        for g in glycan:
            # add the glycan diff to the reference values; .lower means take the lower case.
            i = str('sum_' + g.lower())
            # initialise v
            v = 0
            try:
                v = int(abscomp_row[g]) + int(row[i].iloc[0])
                row.loc[:,g] = v
            except:
                row.loc[:,g]=0

            
            if v > 0:
                comps += (g + str(v))

        # add another column with the concatenated composition.
        row.loc[:,'composition'] = glycanformat.glycan_convert_format(comps,3)

        # ONLY IF this is a reference node,
        # append the other columns of interest from the active row in the reference nodes (abscomp_row)
        # otherwise, just leave blank.
        if int(row.loc[:, 'refnode'])==row.index[0]:
            row = overwrite_df_with_df(abscomp_row, row)
        else:
            row = overwrite_df_with_df(abscomp_row.loc[:,['pep', 'subgraph', 'protein']], row)
        

            
        # append to a dataframe for final output
##        print('row after additions = \n', row)
        df_out = pd.concat([df_out, row], axis=0)        


    # sort the columns
    # add in the critical columns here
    df_relcomp_columns = pd.DataFrame(columns=df_relcomp.columns.tolist() + list(glycan) + ['composition', 'pep','subgraph'])
    # relcomp_col_list = df_relcomp.columns.tolist()
    # cols = relcomp_col_list + [x for x in ref_cols.tolist() if x not in relcomp_col_list]
    cols = combine_two_df_columns(df_relcomp_columns, df_abscomp)
    if df_out.empty:
        raise KeyError("No matches were found when applying the reference nodes to the predicted compositions")

    df_out = df_out[cols]

    
    # do transformations on df_out to prepare
    # it for quant.
    num_chars=10 # number of chars to derive pepgroup
    df_out.loc[:,'pepgroup']=df_out.loc[:,'pep'].apply(lambda x: str(x)[0:num_chars])

    # do more calculations to prep for HUPO
    # because the predcomp may not always have these columns, need to include error handling!
    try:
        df_out['ref'] = df_out.loc[:,'score'].apply(lambda x : "TRUE" if x>0 else np.nan)
    except:
        pass

    # calculate M+H etc
    try:
        df_out['glycanmass'] = df_out['composition'].apply(lambda x : float(glycanformat.glycancomptomass(x)))
        df_out['M+H'] = df_out.loc[:,'mz_node'].apply(lambda x : glycanformat.add_charge_to_neutral_mass(x,charge=1,))
        df_out['TheoretM+H'] = df_out.loc[:, 'pep'].apply(lambda x : glycanformat.peptide_sequence_to_charged_mass(str(x)[2:-2],
                                                                                                                   cysteine_option='iodoacetic',
                                                                                                                   charge_state=1,
                                                                                                                   )) + df_out.loc[:,'glycanmass']
        df_out['delta'] = df_out.loc[:,'M+H'] - df_out.loc[:,'TheoretM+H']
        df_out['ppm'] = df_out['delta'] / df_out['TheoretM+H'] * 1e6
        df_out['accession'] = df_out['protein'].apply(lambda x : x[4:9])
    except:
        pass
    df_out['RT(min)'] = df_out['rt_node'] / 60



    ############ do sumifs based on pepgroup, glycan
    df_groupby = grouppeps(df_out)
    ###########
    
    

##    df_out.to_csv(savefile, index=True, header=True)

    writer  = pd.ExcelWriter(os.path.splitext(savefile)[0]+'.xlsx')
    df_out.to_excel(writer, 'out_predicted_compositions', index_label='node')    
    df_relcomp.to_excel(writer, 'in1_relative_composition')
    df_abscomp.to_excel(writer, 'in2_abs_ref_composition')
    df_info.to_excel(writer, 'INFO inputs and outputs')

    df_groupby.to_excel(pd.ExcelWriter(os.path.splitext(savefile)[0]+'_outPepQuant.xlsx'), 'out2_pepgroup_for_quant')
    
    

## include info on input and output files
    stat = dict(inputfile1=str(relativecomp_file),
                inputfile2=str(abscomp_file),
                outputfile=str(savefile),
                )
    pd.DataFrame(stat, index=['info']).T.to_excel(writer, 'info')
    
    writer.save()

    print('Opening save directory')
    custom_tools.opendir(os.path.dirname(savefile))   
        
    result = dict(resultpath=os.path.splitext(savefile)[0]+'.xlsx',
                  resultdir=os.path.dirname(savefile),
                  quantpath=os.path.splitext(savefile)[0]+'_outPepQuant.xlsx',
                  )
    
    return result
 
    
def grouppeps(df_out):
    df_groupby = df_out.groupby(['pepgroup', 'composition']).sum()[['intensity']]
    df_groupby.reset_index(drop=False, inplace=True)
##    df_groupby['composition']=df_groupby.index.get_level_values('composition')
##    df_groupby['pepgroup']=df_groupby.index.get_level_values('pepgroup')
    df_groupby.loc[:, 'sum_intens_similar_values']=df_groupby.loc[:, 'intensity']
#    df_groupby.loc[:, 'mass']=df_groupby.loc[:, 'composition'].apply(lambda x: glycanformat.glycancomptomass(x))
    df_groupby.loc[:, 'mass']=df_groupby.loc[:, 'composition'].apply(
            lambda x: df_out.loc[df_out.loc[:, 'composition']==x, ['mz_node']].iloc[0,:]
            )
    df_groupby.sort_values(['pepgroup','mass'], inplace=True)
    return df_groupby

def modifyquant():

    f = custom_tools.getfile('get input file with modified compositions', '.xlsx')
    df = pd.read_excel(f, sheetname=0, header=0, index_col='node', na_filter=True, na_values='0')
    df_groupby = grouppeps(df)
    fout = os.path.splitext(f)[0] + '_MODQUANT.xlsx'
    df_groupby.to_excel(pd.ExcelWriter(fout), 'out2_pepgroup_for_quant')


def combine_two_df_columns(df1, df2):
    return df1.columns.tolist() + [x for x  in df2.columns.tolist() if x not in df1.columns.tolist()]


def overwrite_df_with_df(df, df_to_be_overwritten):
    """
    combine the columns from both dfs
    loop through, and if second df's value is nan, then take the first df's value
    :param df:
    :param df_to_be_overwritten:
    :return:
    """
    from collections import OrderedDict
    cols = combine_two_df_columns(df, df_to_be_overwritten)
    # must use ordereddict to preserve order in which the keys were added.
    vlist = OrderedDict()
    # this updates using df_overwrite first, and failing that, using df.
    # this returns a dictionary
    for c in cols:
        try:
            vlist[c] = df_to_be_overwritten[c].values[0]
        except:
            vlist[c] = df[c].values[0]
    # convert the dict to dataframe
    # attempt to set the node as the index, but failing that, default to index = 0
    try:
        return pd.DataFrame(vlist, index=df_to_be_overwritten.index).set_index('node', drop=True)
    except:
        return pd.DataFrame(vlist, index=df_to_be_overwritten.index)


if __name__ == "__main__":
    applyindirectcompositions()
