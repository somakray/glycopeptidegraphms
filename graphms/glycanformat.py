"""

"""

import re
import sys, os
import pandas as pd

FORMAT_OPTIONS = {1 : dict(N='N', H='H', F='F', S='S', end=''),
                  2 : dict(N="HexNAc", H="Hex", F="Fuc", S="NeuAc", end=""),
                  3 : dict(N="HexNAc(", H=")Hex(", F=")Fuc(", S=")NeuAc(", end=")"),
                  }

GLYCANMASS_ABBREV = {'Hex':162.05282,
                     'HexNAc':203.07937,
                     'HexN':161.06881,
                     'HexA':176.03209,
                     'dHex':146.05791,
                     'Pent':132.0423,
                     'NeuAc':291.0954,
                     'NeuGc':307.0903,
                     'KDN':250.0689,
                     'Acetate':42.0106,
                     'Phosphate':79.9663,
                     'Sulphate':79.9568,
                     'Na':22.989768,
                     'K':38.963707,
                     'proton':1.00727,
                     }

GLYCAN_FORMAT_1 = {'HexNAc':'N',
                   'Hex':'H',
                   'NeuAc':'S',
                   'NeuGc':'G',
                   'Ac':'A',
                   'Phos':'P',
                   'Fuc':'F',
                   'Su':'Su'}

GLYCANMASS =   {'H':162.05282,
                'F':146.05791,
                'S':291.09542,
                'N':203.07937,
                'proton':1.00727,
                }

ARRAY_NAMES = \
        [["N",
           "F",
           "H",
           "S"],
               ["(?:(?:HexNAc)|(?:GlcNAc))",
           "(?:(?:dHex)|(?:Fuc))",
           "(?:(?:(?:(?<!d)(?:Hex))|(?:Man))(?!NAc))",
           "NeuAc"],
               ["(?:(?:(?:HexNAc)|(?:GlcNAc))\()",
           "(?:(?:(?:dHex)|(?:Fuc))\()",
           "(?:(?:(?<!d)(?:(?:Hex)|(?:Man))(?!NAc))\()",
           "(?:NeuAc\()"],
               ["(?:\((?:(?:HexNAc)|(?:GlcNAc))\))",
           "(?:\((?:(?:Deoxyhexose)|(?:Fuc)|(?:Fucose))\))",
           "(?:\((?:(?:Hex)|(?:Man))\))",
           "\(NeuAc\)"],
               ]



re_list = ["^([A-Z]\d)", "^([A-Z]{3,6}\d)", "^([A-Z]{3,6}\(\d)", "\(.*\)\d"]

# def glycan_convert_format(s='', iformat = 1):
#
#     if s=='':
#         sys.exit('Fatal error. no string passed to glycan convert format function!')
#
#     # this makes a dict of NFHS
#     temp_comp = glycan_get_comp_abbrev(s)
# ##    print(temp_comp)
#
#     if str(iformat) not in '123':
#         print('iformat was not recognised...defaulting to = 1')
#         iformat=1
#
#     out = ''
#
#     for x in 'NHFS':
#         if str(temp_comp[x]) == '0':
#             continue
#         out = out + str(FORMAT_OPTIONS[iformat][x]) + str(temp_comp[x])
#         # the output is a string with the composition
#
#     out = out + FORMAT_OPTIONS[iformat]['end']
#
#
#     return out

    
def systemfolder(relative_path):

    if getattr(sys, 'frozen', False):
        return os.path.join(sys._MEIPASS, relative_path)
    else:
        return relative_path

def glycan_get_comp(str_in):
    if str_in == '':
        str_in = 'Hex0'

    symbol_and_number_duple_list = parse_string_with_known_format(str_in)

    return dict(symbol_and_number_duple_list)



# def glycan_get_comp(s):
#
#
#     # this takes a string and converts it to a 4-dictionary of compositions
# ##    print('glycan get comp started')
#
#     if s == '':
#         # print('no input was passed for conversion! Quitting...x')
#         return dict(N=0, H=0, S=0, F=0)
#
#     re_list = ["^([A-Z]\d)", "^([A-Z]{3,6}\d)", "^([A-Z]{3,6}\(\d)", "\(.*\)\d"]
#
#     anames = [["N",
#                    "F",
#                    "H",
#                    "S"],
#                ["(?:(?:HexNAc)|(?:GlcNAc))",
#                     "(?:(?:dHex)|(?:Fuc))",
#                     "(?:(?:(?:(?<!d)(?:Hex))|(?:Man))(?!NAc))",
#                     "NeuAc"],
#                ["(?:(?:(?:HexNAc)|(?:GlcNAc))\()",
#                     "(?:(?:(?:dHex)|(?:Fuc))\()",
#                     "(?:(?:(?<!d)(?:(?:Hex)|(?:Man))(?!NAc))\()",
#                     "(?:NeuAc\()"],
#                ["(?:\((?:(?:HexNAc)|(?:GlcNAc))\))",
#                     "(?:\((?:(?:Deoxyhexose)|(?:Fuc)|(?:Fucose))\))",
#                     "(?:\((?:(?:Hex)|(?:Man))\))",
#                     "\(NeuAc\)" ],
#               ]
#
#
#     #first, determine which type the input string is.
#     for index, x in enumerate(re_list):
# ##        print(index, ' _ ', x)
#         m = re.search(x, s, flags=re.I)
#         try:
#             # this next line will fail if no match
#             # it MUST NOT BE COMMENTED AWAY!
#             m[0]
#             i_case=index
#             break
#
#         except:
#             continue
#
#     # after the matching, if there is still no i_case, then abort (input was not a glycan!)
#     try:
#         i_case
#     except NameError:
#         # print('Input string was not blank but also not a glycan! Returning a composition of all zeroes.')
#         return dict(N=0, H=0, S=0, F=0)
#
#     #get compositions and sum any repeats
# ##    print('i case  = ', i_case)
#     a_comp = [0,0,0,0] # order is NFHS, as defined in anames above.
#     for n in range(0,4):
# ##        print(anames[i_case][n] + "\d+")
#         r = re.findall(anames[i_case][n] + "\d+", s, flags=re.I)
#
# ##        print('matches = ', r)
#
#         for index, m in enumerate(r):
#             a_comp[n] = a_comp[n] + int(re.search('\d+', m)[0])
#
#
#     a_comp = dict(zip('NFHS', a_comp))
#
# ##    print('a_comp = ', a_comp)
#     return a_comp
#
#
# def glycan_get_comp_abbrev(s):
#     # this takes a string and converts it to a 4-dictionary of compositions
#     ##    print('glycan get comp started')
#
#     if s == '':
#         # print('no input was passed for conversion! Quitting...x')
#         return dict(zip(GLYCANMASS_ABBREV.keys(), [0] * len(GLYCANMASS_ABBREV)))
#
#
#
#     # first, determine which type the input string is.
#     for index, x in enumerate(re_list):
#         ##        print(index, ' _ ', x)
#         m = re.search(x, s, flags=re.I)
#         try:
#             # this next line will fail if no match
#             # it MUST NOT BE COMMENTED AWAY!
#             m[0]
#             i_case = index
#             break
#
#         except:
#             continue
#
#     # after the matching, if there is still no i_case, then abort (input was not a glycan!)
#     try:
#         i_case
#     except NameError:
#         # print('Input string was not blank but also not a glycan! Returning a composition of all zeroes.')
#         return dict(N=0, H=0, S=0, F=0)
#
#     # get compositions and sum any repeats
#     ##    print('i case  = ', i_case)
#     a_comp = [0, 0, 0, 0]  # order is NFHS, as defined in anames above.
#     for n in range(0, 4):
#         ##        print(anames[i_case][n] + "\d+")
#         r = re.findall(ARRAY_NAMES[i_case][n] + "\d+", s, flags=re.I)
#
#         ##        print('matches = ', r)
#
#         for index, m in enumerate(r):
#             a_comp[n] = a_comp[n] + int(re.search('\d+', m)[0])
#
#     a_comp = dict(zip('NFHS', a_comp))
#
#     ##    print('a_comp = ', a_comp)
#     return a_comp

# def glycancomptomass(s, reducing=0.):
#     comp = glycan_get_comp(s)
#     mass = 0.
#
#     for g in 'NFHS':
#         mass += comp[g] * GLYCANMASS[g]
#
#     return round(mass,4) + reducing

def glycancomptomass(str_in, reducing=0.):
    """converts a glycan string into neutral mass based on SNFG symbols.

    :param str_in:
    :param reducing: mass to add on to final result, i.e. the reducing end modification mass
    :return:
    """
    if str_in == '': return 0.
    symbol_and_number_duple_list = parse_string_with_known_format(str_in)
    abbrev_list = get_standard_abbrev_from_glycan_duple_list(symbol_and_number_duple_list)
    # zip together the standard abbrevs with the same number; ie replace str_in with standard abbrevs
    duple_list_with_unique_standard_abbrevs = convert_glycan_duples_to_standard_abbrev_with_groupby(symbol_and_number_duple_list,abbrev_list, as_int=True)


    mass = 0.
    for glycan, quant in duple_list_with_unique_standard_abbrevs:
        mass += float(get_mass_from_mono_table_by_glycan(glycan)) * float(quant)

    return round(mass,4) + reducing


def peptide_sequence_to_charged_mass(seq, cysteine_option='iodoacetic', charge_state=0):
    """

    :param seq:
    :param cysteine_option:
    :param charge_state:
    :return:
    """
    if not isinstance(seq, str): raise TypeError

    if cysteine_option=='iodoacetic':
        cys_mass = 103.00919 + 57.0215
    elif cysteine_option=='iodoacetamide':
        cys_mass = 103.00919 + 57.0215 + 0.984
    else:
        cys_mass = 103.00919

    residue_mass = dict(A=71.03711,
                        R=156.10111,
                        N=114.04293,
                        D=115.02694,
                        C=cys_mass,
                        Q=128.05858,
                        E=129.04259,
                        G=57.02146,
                        H=137.05891,
                        I=113.08406,
                        L=113.08406,
                        K=128.09496,
                        M=131.04049,
                        F=147.06841,
                        P=97.05276,
                        S=87.03203,
                        T=101.04768,
                        W=186.07931,
                        Y=163.06333,
                        V=99.06841,
                        )

    # first add the N (H) and C (OH) termini
    result = 18.010565

    for letter in seq:
        result += residue_mass[letter.upper()]

    return add_charge_to_neutral_mass(result, charge=charge_state)


def add_charge_to_neutral_mass(mass, charge):
    return (mass + (GLYCANMASS['proton'] * charge)) / charge


def get_test_sequence():

    test_sequence = {'comp_1':{'format_1':'N1H2F3S4G5A6P7Su8',
                               'format_2':'HexNAc1Hex2Fuc3NeuAc4NeuGc5Ac6Phos7Su8',
                               'format_3':'HexNAc(1)Hex(2)Fuc(3)NeuAc(4)NeuGc(5)Ac(6)Phos(7)Su(8)',
                               'format_4':'(HexNAc)1(Hex)2(Fuc)3(NeuAc)4(NeuGc)5(Ac)6(Phos)7(Su)8'},
                     'comp_2':{'format_3':'4eleg(1)Bac(2)AltN(3)AllA(4)dGul(5)'},
                     'comp_3':{'format_3':'GlcNAc1Phos2Acetate3'},
                     }
    answer_sequence = {'comp_1':'HexNAc(1)Hex(2)Fuc(3)NeuAc(4)NeuGc(5)Ac(6)Phos(7)Su(8)',
                       'comp_2':'4eleg(1)Bac(2)AltN(3)AllA(4)dGul(5)',
                       'comp_3':'GlcNAc(1)Phos(2)Ac(3)'}

    return test_sequence, answer_sequence


def get_glycan_format_from_string(str_in):
    """from a string, get (1) the bracketing convention and (2) the glycan naming convention
    initial options were: x1y1, xxx1yyy1, xxx(1, (xxx)1
    (071019)
    But I have since moved on from needing to parse it in this way, preferring instead to
    split the string into its components and matching the whole values to a list of synonyms
    This means that the only thing of importance is to establish the bracketing convention

    i.e.:
    (1) are brackets around numbers or glycan symbols?


    :param str_in:
    :return:
    """
    # old re list
    # re_list = ["^([A-Z,a-z]\d)", "^([A-Z,a-z]{2,6}\d)", "^([1-9,A-Z,a-z]{3,6}\(\d)", "^\(.*\)\d"]

    # new re list with either numbers inside brackets (ans=1), or outside brackets (ans=2), or no brackets (ans=3) or undefined (ans=4)
    re_list = ["(\(\d+\))", "(\)\d+)", "(^[^\(\)]+$)", "(.*)"]


    for i,r in enumerate(re_list):
        matches = re.findall(r, str_in)
        # print(matches)
        if matches != []: break

    return i+1

def parse_string_with_known_format(str_in, glycan_format_number=None):
    """Parse out all the component glycan names and quantities into [(glycan,number),(glycan,number),...]

    :param str_in:
    :param glycan_format_number:
    :return:
    """
    ans=''
    if glycan_format_number is None:
        glycan_format_number = get_glycan_format_from_string(str_in)

    if glycan_format_number==1:
        # i.e. numbers inside brackets
        ans = re.findall(r"([1-9,A-Z,a-z,-]+)(?:\()(\d+)", str_in)
        pass

    elif glycan_format_number == 2:
        # option 2, names inside brackets
        ans = re.findall(r"(?:\()([1-9,A-Z,a-z,-]+)(?:\))(\d+)", str_in)

    elif glycan_format_number == 3:
        # option 3, no brackets
        ans = re.findall(r"([A-Z,a-z,-]+)(\d+)", str_in)

    else:
        ans=[]

    ## OLD regex options
    # if glycan_format_number==1:
    #     ans = re.findall(r'([A-Z,a-z]{1,2})(\d+)', str_in)
    #
    # elif glycan_format_number==2:
    #     ans = re.findall(r"([A-Z,a-z]+)(\d+)", str_in)
    #
    # elif glycan_format_number==3:
    #     ans = re.findall(r"([1-9,A-Z,a-z]+)(?:\()(\d+)", str_in)
    #
    # elif glycan_format_number==4:
    #     ans = re.findall(r"(?:\()([1-9,A-Z,a-z]+)(?:\))(\d+)", str_in)

    # convert the second value, ie the number to integer. Will fail if non integer as second number is passed.
    return [(x[0],int(x[1])) for x in ans]

def get_glycan_table():

    f = r'O:\Analytics\Matt\034 platypus glycans\dev for acetylation\monosaccharide tables.xlsx'
    df_mono = pd.read_excel(f, header=0, index_col=None)

    return df_mono


def expand_monosaccharide_table():
    df_mono = get_glycan_table()

    # expand the monosaccharide table
    # note that this list will have strings and NaNs
    glycan_list = [x for i, x in df_mono.loc[:, 'synonym'].iteritems()]

    new_rows = pd.DataFrame()

    for i,x in enumerate(glycan_list):
        if type(x)==float: continue

        list_of_glycans = x.split(",")

        for syn in list_of_glycans:
            if syn == '': continue
            mod_row = df_mono.iloc[i, :]
            mod_row['glycan'] = syn
            new_rows = new_rows.append(mod_row)

    final_df = df_mono.append(new_rows)
    final_df = final_df.reset_index(drop=True)

    writer = pd.ExcelWriter(systemfolder('resources/monosaccharides.xlsx'))
    final_df.to_excel(writer)
    writer.save()

    pd.to_pickle(final_df, systemfolder('resources/monosaccharides.pkl'))


def get_monosaccharide_table(f=systemfolder('resources/monosaccharides.pkl')):
    return pd.read_pickle(f)


def parse_glycans_from_table(list_of_tuples):
    df_mono = MONO_TABLE # get_monosaccharide_table()

    results=[]
    results_append = results.append
    for gly, quant in list_of_tuples:
        try:
            results_append(df_mono.loc[df_mono.loc[:, 'glycan'].str.lower() == gly.lower(), 'abbrev'].values[0])
        except IndexError:
            results_append(gly)


    # results = [df_mono.loc[df_mono.loc[:,'glycan'].str.lower()==gly.lower(), 'abbrev'].values[0] for gly, quant in list_of_tuples]

    return results


def get_standard_abbrev_from_glycan_duple_list(list_of_tuples):
    """from a list of tuples [(glycan_name, number),(),...], get a unique list of all the STANDARD glycan names based on the monosaccharide abbreviation table

    :param list_of_tuples:
    :return:
    """
    # df_mono = MONO_TABLE # get_monosaccharide_table()

    list_of_glycans_found=[]
    results_append = list_of_glycans_found.append
    for gly, quant in list_of_tuples:
        quant=str(quant)
        try:
            # results_append(df_mono.loc[df_mono.loc[:, 'glycan'].str.lower() == gly.lower(), 'abbrev'].values[0])
            results_append(get_abbrev_from_mono_table_by_glycan(gly))
        except IndexError:
            results_append(gly)


    # results = [df_mono.loc[df_mono.loc[:,'glycan'].str.lower()==gly.lower(), 'abbrev'].values[0] for gly, quant in list_of_tuples]

    # return the unique set
    return list_of_glycans_found

def write_composition_by_format(list_of_abbreviated_tuples, format=3):
    result = []
    result_append=result.append
    if format==1:
        for gly, quant in list_of_abbreviated_tuples:
            try:
                result_append('{}{}'.format(GLYCAN_FORMAT_1[gly],quant))
            except:
                result_append('{}{}'.format(gly, quant))
        return ''.join(result)
    elif format==2:
        return ''.join(['{}{}'.format(gly, quant) for gly, quant in list_of_abbreviated_tuples])
    elif format==3:
        return ''.join(['{}({})'.format(gly, quant) for gly, quant in list_of_abbreviated_tuples])
    elif format==4:
        return ''.join(['({}){}'.format(gly, quant) for gly, quant in list_of_abbreviated_tuples])

def convert_glycan_duples_to_standard_abbrev_with_groupby(symbol_and_number_duple_list, abbrev_list, as_int=False):
    """takes the original duple list and applies standard abbreviations AND sums up identical glycan names.

    :param symbol_and_number_duple_list:
    :param abbrev_list:
    :param as_int: Whether to returns quantities in either integer, or else string by default
    :return:
    """


    duple_list_with_standard_abbrevs = list(zip(abbrev_list, [x[1] for x in symbol_and_number_duple_list]))

    if as_int:
        answer_list = pd.DataFrame(duple_list_with_standard_abbrevs).groupby(0, axis=0, as_index=False).sum().apply(
            lambda x: (x[0], int(x[1])), axis=1).values
    else:
        answer_list = pd.DataFrame(duple_list_with_standard_abbrevs).groupby(0, axis=0, as_index=False).sum().apply(
            lambda x: (x[0], str(x[1])), axis=1).values

    return answer_list

def glycan_convert_format(str_in='', iformat = 3):
    """Converts a glycan string to a iformat of choice.

    :param str_in:
    :param iformat:
    :return:
    """
    if str_in == '': return ''
    try:
        symbol_and_number_duple_list = parse_string_with_known_format(str_in)
        abbrev_list = get_standard_abbrev_from_glycan_duple_list(symbol_and_number_duple_list)
        # replace str_in with standard abbrevs and collect the sum of each glycan, to deal with repeated glycan compositions
        duple_list_with_unique_standard_abbrevs = convert_glycan_duples_to_standard_abbrev_with_groupby(symbol_and_number_duple_list,abbrev_list)
    except KeyError as e:
        print('Incorrect glycan string passed to glycan_convert_format; returning a blank.', e)
        return ''

    return write_composition_by_format(duple_list_with_unique_standard_abbrevs, iformat)


def get_mass_from_mono_table_by_glycan(gly):
    """this gets a mass from the monosaccharide table give a full glycan name

    :param gly:
    :return:
    """
    try:
        mass = MONO_TABLE.loc[MONO_TABLE.loc[:, 'glycan'].str.lower() == gly.lower(), 'mass'].values[0]
    except IndexError:
        # indicates that there was a mistake in the glycan name parsed or in the parser.
        print('Index Error encountered at glycan=', gly)

    return mass


def get_abbrev_from_mono_table_by_glycan(gly):
    """this gets an Abbreviated name from the monosaccharide table given a full glycan name

    :param gly:
    :return:
    """
    return MONO_TABLE.loc[MONO_TABLE.loc[:, 'glycan'].str.lower() == gly.lower(), 'abbrev'].values[0]


def test_conversion():
    test_sequence, answer_sequence = get_test_sequence()

    for k in test_sequence.keys():

        for k2 in test_sequence[k]:
            str_in = test_sequence[k][k2]
            glycan_format = get_glycan_format_from_string(str_in)
            print(glycan_format)

            ans = parse_string_with_known_format(str_in)

            abbrev_list = get_standard_abbrev_from_glycan_duple_list(ans)

            result = list(zip(abbrev_list, [x[1] for x in ans]))

            s = write_composition_by_format(result, 3)

            print(result)
            print(s)

            print('Did test pass? {}'.format(s == answer_sequence[k]))

def test_glycan_format_from_string():
    test_dict = {'Hex(1)HexNAc(1)':1,
                 '(Hex)1(HexNAc)1':2,
                 'H1N1':3,
                 }

    for key in test_dict.keys():
        ans = get_glycan_format_from_string(key)
        result = True if ans == test_dict[key] else False
        print('ans={}; expected={}; result={}'.format(ans,test_dict[key],result))
        assert ans == test_dict[key]


def test_comp_to_mass():
    test_string = 'Glc(2)Man(3)Gal(4)Gul(5)Alt(6)All(7)Tal(8)Ido(9)Hex(10)GlcNAc(11)ManNAc(12)GalNAc(13)GulNAc(14)AltNAc(15)AllNAc(16)TalNAc(17)IdoNAc(18)HexNAc(19)GlcN(20)ManN(21)GalN(22)GulN(23)AltN(24)AllN(25)TalN(26)IdoN(27)HexN(28)GlcA(29)ManA(30)GalA(31)GulA(32)AltA(33)AllA(34)TalA(35)IdoA(36)HexA(37)Qui(38)Rha(39)dGul(40)dAlt(41)dTal(42)Fuc(43)dHex(44)QuiNAc(45)RhaNAc(46)dGulNAc(47)dAltNAc(48)dTalNAc(49)FucNAc(50)dHexNAc(51)Ara(52)Lyx(53)Xyl(54)Rib(55)Pent(56)NeuAc(57)NeuGc(58)Kdn(59)Ac(60)Phos(61)Su(62)Na(63)K(64)proton(65)Pse(66)Leg(67)Aci(68)4eLeg(69)Di-deoxynonulosonate(70)Kdo(71)Bac(72)LDmanHep(73)Dha(74)DDmanHep(75)MurNAc(76)MurNGc(77)Mur(78)Api(79)Fru(80)Tag(81)Sor(82)Psi(83)Oli(84)Tyv(85)Abe(86)Par(87)Dig(88)Col(89)ddHex(90)Glc(91)Man(92)Gal(93)Gul(94)Alt(95)All(96)Tal(97)Ido(98)Hex(99)Hex(100)GlcNAc(101)GlcNAc(102)ManNAc(103)ManNAc(104)GalNAc(105)GalNAc(106)GulNAc(107)GulNAc(108)AltNAc(109)AltNAc(110)AllNAc(111)AllNAc(112)TalNAc(113)TalNAc(114)IdoNAc(115)IdoNAc(116)HexNAc(117)HexNAc(118)GlcN(119)ManN(120)GalN(121)GulN(122)AltN(123)AllN(124)TalN(125)IdoN(126)HexN(127)GlcA(128)ManA(129)GalA(130)GulA(131)AltA(132)AllA(133)TalA(134)IdoA(135)HexA(136)Qui(137)Rha(138)dGul(139)dAlt(140)dTal(141)Fuc(142)Fuc(143)dHex(144)QuiNAc(145)RhaNAc(146)dGulNAc(147)dAltNAc(148)dTalNAc(149)FucNAc(150)dHexNAc(151)Ara(152)Lyx(153)Xyl(154)Rib(155)Pent(156)Pent(157)Pent(158)NeuAc(159)NeuAc(160)NeuAc(161)NeuAc(162)NeuAc(163)NeuGc(164)NeuGc(165)NeuGc(166)NeuGc(167)Kdn(168)Ac(169)Ac(170)Phos(171)Phos(172)Su(173)Su(174)Na(175)K(176)Pse(177)Leg(178)Aci(179)4eLeg(180)Kdo(181)Kdo(182)Bac(183)LDmanHep(184)Dha(185)DDmanHep(186)MurNAc(187)MurNGc(188)Mur(189)Api(190)Fru(191)Tag(192)Sor(193)Psi(194)Oli(195)Tyv(196)Abe(197)Par(198)Dig(199)Col(200)ddHex(201)ddHex(202)'
    test_value = 3572665.2046
    ans = glycancomptomass(test_string)
    print('expected={}; observed={}'.format(test_value,ans))
    assert abs(ans - test_value) < 0.000001

    return

def test():
    test_sequence, answer_sequence = get_test_sequence()

    test_list = []

    for k in test_sequence.keys():

        for k2 in test_sequence[k]:
            print('\n\niteration:{}-{}'.format(k,k2))
            str_in = test_sequence[k][k2]
            glycan_format = get_glycan_format_from_string(str_in)

            print('test_input =', str_in)
            print('recognised_glycan_format =', glycan_format)

            ans = parse_string_with_known_format(str_in)

            abbrev_list = parse_glycans_from_table(ans)

            result = list(zip(abbrev_list, [x[1] for x in ans]))

            write_format = write_composition_by_format(result, 3)

            boolean_test_answer = write_format == answer_sequence[k]

            print('composition = {}\n'.format(result))
            print('expected = {}\nresult   = {}'.format(answer_sequence[k],write_format))
            print('Did test pass? {}'.format(boolean_test_answer))

    # check the test list
    print('\n\n#############\nDid all tests pass? {}\n#############'.format(sum(test_list)==len(test_list)))

### define the monosacc table as a constant
MONO_TABLE = get_monosaccharide_table()
MONO_TABLE_INDEXED = MONO_TABLE.set_index('glycan', drop=True)


if __name__ == '__main__':
    # TEST_SEQUENCE = ''.join([k+str(i+1) for i,k in enumerate(GLYCANMASS_ABBREV.keys())])
    # print(TEST_SEQUENCE)
    # glycan_convert_format(TEST_SEQUENCE, 1)

    # for k in range(1,5):
    #     x = glycan_convert_format('Bob5Hex5Ac2NeuAc3X4Phos6Phos4',iformat=k)
    #     print('x=',x)
    test()




