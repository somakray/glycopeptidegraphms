import tkinter as tk
from tkinter import ttk, messagebox
import pygubu, pygubu.builder
from pygubu.builder import ttkstdwidgets, tkstdwidgets
from pygubu.builder.widgets import dialog, editabletreeview, scrollbarhelper, scrolledframe, tkscrollbarhelper, tkscrolledframe
from graphms import custom_tools, applyindirectcompositions, predictcompositions, \
    graphanalysis, graphquant, bokehgraphms, cleanconsensus
import ast
from os import startfile
import pdb, sys, os

def setentryvalue(obj, val):
    obj.delete(0, 'end')
    obj.insert(0, val)

def sacrificial():
    pygubu.builder
    pdb

def systemfolder(relative_path):

    if getattr(sys, 'frozen', False):
        return os.path.join(sys._MEIPASS, relative_path)
    else:
        return relative_path
      

class Application:
    def __init__(self,master):
        self.version = "2.01 30-Nov-2018"
      
        guipath= systemfolder('resources/gui.ui')
        master.focus_set()
               
        self.master=master
        self.builder=builder=pygubu.Builder()
        builder.add_from_file(guipath)
        self.mainwindow = builder.get_object('mainwindow', master)
        builder.connect_callbacks(self)

        self.builder2 = pygubu.Builder()
        self.builder2.add_from_file(guipath)
        self.newwindow = tk.Toplevel(self.mainwindow)
        self.scrollhelper = self.builder2.get_object('scrollhelper', self.newwindow)
        self.text_console = self.builder2.get_object('text_console', self.newwindow)
        
        self.newwindow.protocol("WM_DELETE_WINDOW", lambda: None)
        self.builder2.connect_callbacks({})
        self.newwindow.geometry('+0+400')
        

        master.protocol("WM_DELETE_WINDOW", self.on_close_window)

        self.entry_consensus_path= self.builder.get_object('entry_consensus_path', self.master)
        self.entry_graphanalysis_parameters= self.builder.get_object('entry_graphanalysis_parameters', self.master)
        self.entry_graphanalysis_consensus= self.builder.get_object('entry_graphanalysis_consensus', self.master)
        self.entry_graphanalysis_jump= self.builder.get_object('entry_graphanalysis_jump', self.master)
        self.entry_plotsub_file= self.builder.get_object('entry_plotsub_file', self.master)
        self.entry_quant_threshold = self.builder.get_object('entry_quant_threshold', self.master)
        
        self.scale_plotsub_max= self.builder.get_object('scale_plotsub_max', self.master)
        self.scale_plotsub_min= self.builder.get_object('scale_plotsub_min', self.master)
        
        self.checkbutton_show_edges= self.builder.get_object('checkbutton_show_edges', self.master)        
        self.checkbutton_output_format = self.builder.get_object('checkbutton_output_format', self.master)
        
        self.predictcompositions_button = self.builder.get_object('predictcompositions_button', self.master)
        self.openref_button = self.builder.get_object('openref_button', self.master)
        self.openpredcomp_button = self.builder.get_object('openpredcomp_button', self.master)        
        self.applycompositions_button = self.builder.get_object('applycompositions_button', self.master)
        self.openabscomp_button = self.builder.get_object('openabscomp_button', self.master)
        self.showhelp_button = self.builder.get_object('showhelp_button', self.master)
        self.about_button = self.builder.get_object('about_button', self.master)
        

        self._applystyles()

        setentryvalue(self.entry_consensus_path, '')
        setentryvalue(self.entry_graphanalysis_consensus, '')
        setentryvalue(self.entry_graphanalysis_jump, '')
        setentryvalue(self.entry_graphanalysis_parameters, '')
        setentryvalue(self.entry_plotsub_file, '')
             
        self.scale_plotsub_max.set(40)
        self.scale_plotsub_min.set(5)
        
#        self.builder.create_variable('plotsubformat')
#        self.builder.create_variable('var_checkbutton_show_edges')
#        self.builder.create_variable('applycompositions_usecurrent')        
        
#        self.var_plotsubformat = builder.get_variable('plotsubformat') 
#        self.var_plotsubformat.set('canvas')
        self.var_showedges = builder.get_variable('var_checkbutton_show_edges')
        self.var_applycompositions_usecurrent= builder.get_variable('applycompositions_usecurrent') 
        self.var_quant_usecurrent= builder.get_variable('quant_usecurrent') 
        self.var_checkbutton_open_folder = builder.get_variable('var_checkbutton_open_folder')
        
        self.var_combobox_scaling_option = builder.get_variable('var_combobox_scaling_option')        
        self.var_combobox_scaling_option.set('3_log_all')
        
        self.var_checkbutton_output_format = builder.get_variable('var_checkbutton_output_format')
#        self.var_plotsubformat = self.builder.get_variable('plotsubformat')
        
        # this also sets a variable to self to make it easier to reference
#        self.var_checkbutton_export_csv = builder.get_variable('var_checkbutton_export_csv')
#        self.var_checkbutton_show_edges = builder.get_variable('var_checkbutton_show_edges')                     
#        print(builder.get_variable('var_checkbutton_export_csv').get())        

        style = ttk.Style()
        style.configure('.', background = '#ffffff')

        

    def callback(*args):
        print('called back')

    def updateconsole(self, texttoinsert):
        self.text_console.insert('end', '\n')
        self.text_console.insert('end', texttoinsert)
        self.newwindow.lift()

     

    def on_quit_button_click(self):
        print('quitting')
        
        self.master.withdraw()
        self.master.quit()
        self.master.destroy()


    def on_close_window(self):
        
        self.on_quit_button_click()





    def on_consensus_getfile_button_click(self):
        setentryvalue(self.entry_consensus_path,
                custom_tools.getfile('Select the consensus CSV file to format', '.csv')
                )


    def on_consensus_button_click(self):

        if len(self.entry_consensus_path.get()) < 1:
            self.consensus_result = cleanconsensus.clean()
        else:
            self.consensus_result = cleanconsensus.clean(file_list=[self.entry_consensus_path.get()])

        self.updateconsole('\ncleanconsensus result=\n')
        self.updateconsole(self.consensus_result)

    def on_graphanalysis_usecurrentfile_button_click(self):
        setentryvalue(self.entry_graphanalysis_consensus, self.consensus_result[0])
        self.updateconsole('Using current file = \n{}'.format(self.consensus_result[0]))


    def on_graphanalysis_getjump_button_click(self):
        setentryvalue(self.entry_graphanalysis_jump,
                custom_tools.getfile('Choose CSV file containing glycan differences', '.csv')
                )


    def on_graphanalysis_getfile_button_click(self):
        setentryvalue(self.entry_graphanalysis_consensus,
                custom_tools.getfile('Select the formatted consensus CSV file to apply GraphMS', '.csv')
                )


    def on_graphanalysis_getparameter_button_click(self):
        setentryvalue(self.entry_graphanalysis_parameters,
                custom_tools.getfile('Select the INI parameter file to use', '.ini')
                )


    def on_graphanalysis_button_click(self):
        self.updateconsole('Running GraphMS algorithm. This may take up to 10 minutes for large datasets, and this program may seem to be unresponsive.\n\nObserve the black console for progress messages.')
        try:
            self.graphms_result = graphanalysis.dographanalysis(
                inipath=(self.entry_graphanalysis_parameters.get() if len(self.entry_graphanalysis_parameters.get())>0 else ''),
                consensuspath=(self.entry_graphanalysis_consensus.get() if len(self.entry_graphanalysis_consensus.get())>0 else ''),
                jumplistpath=(self.entry_graphanalysis_jump.get() if len(self.entry_graphanalysis_jump.get())>0 else ''),
                )
            self.updateconsole('GraphMS completed')
        except:
            self.updateconsole('GraphMS encountered an error. Check the black console for information.')
            raise

    def on_plotsub_getgroupings_button_click(self):
        pass


    def on_plotsub_usefilelist_button_click(self):
        setentryvalue(self.entry_plotsub_file, str(list(self.graphms_result['outgraphpathlist'])))



    def on_plotsub_getfile_button_click(self):
        setentryvalue(self.entry_plotsub_file,
                str(list(custom_tools.getfiles('Choose graphml files to plot overlaid', '.graphml')))
                )


    def on_plotsub_button_click(self):

        q = bokehgraphms.subgraphfigure()
        options = dict(
                    export_csv=False,
                    circle_size_max=self.scale_plotsub_max.get(),
                    circle_size_min=self.scale_plotsub_min.get(),
                    show_edges=self.var_showedges.get(),
                    output_format='svg' if self.var_checkbutton_output_format.get() else 'canvas',
                    style='',
                    output_html_file_path=None,
                    scaling=3 if self.var_combobox_scaling_option.get()=='' else int(self.var_combobox_scaling_option.get()[0]),
                    )
        file_list = ast.literal_eval(self.entry_plotsub_file.get()) if len(self.entry_plotsub_file.get()) > 0 else []
#        pdb.set_trace()
        q.add_subgraphs_to_plot(file_list, options)
        p = q.plot()

        if bool(self.var_checkbutton_open_folder.get()):
            custom_tools.opendir(os.path.dirname(q.graphmls[0]))



    def on_predictcompositions_buttonclick(self):

        result = predictcompositions.predictcompositions()
        self.const_predcomppath = result['predcomp']
        self.const_refnodepath = result['refnodes']

        # toggle the state of the deactivated buttons
        self.togglepredictionbuttons('normal')
        pass

    def togglepredictionbuttons(self, state):
        #https://stackoverflow.com/questions/16046743/how-to-change-tkinter-button-state-from-disabled-to-normal

#        state2 = 'disabled' if state=='normal' else 'normal'
#        if (self.openpredcomp_button['state']==state2) : self.openpredcomp_button['state'] = state
#        if (self.openref_button['state']==state2) : self.openref_button['state'] = state

        ## the .state format is required for ttk rather than tk!
        self.openpredcomp_button.state(["!disabled"])
        self.openref_button.state(["!disabled"])
#        self.openpredcomp_button['state'] = 'normal' if (self.openpredcomp_button['state']=='disabled') else 'disabled'
#        self.openref_button['state'] = 'normal' if (self.openref_button['state']=='disabled') else 'disabled'
        pass


    def on_openpredcomp_button_click(self):
        startfile(self.const_predcomppath)
        pass

    def on_openref_button_click(self):
        startfile(self.const_refnodepath)
        pass

    def on_showhelp_button_click(self):
        helpmessage = 'For more details please read the user guide\n\n1. Open the reference nodes XLSX.\n\n2. Option A - fill in the name of the peptide(pep) for each reference node after you have sequenced using MS2/database-search. OR\n\nOption B - You may sequence any node in the subgraph and can enter its composition (in the NHFS columns) and let the program know by updating the new mz_node and rt_node AND set node=0. Do not alter refnode or mz_refnode.\n\n3. Click the button to Apply Compositions and use your updated reference nodes.'
        messagebox.showinfo('Help on reference nodes', helpmessage)

    def on_about_button_click(self):
        helpmessage = 'GlycopepideGraphMS v' + str(self.version) + '\n\nMade by Matthew SF Choo\n\nAt the Bioprocessing Technology Institute, in the Agency for Science, Technology and Research of Singapore\n\nFor questions email:\nmatthew_choo@bti.a-star.edu.sg or choo.matt@gmail.com\n\nFor more help, please check the documentation and user guide.\n\nThe algorithms used are protected by a provisional patent (Patent Pending), but this software is available for academic use.'
        messagebox.showinfo('About GlycopeptideGraphMS', helpmessage)



    def on_applycompositions_button_click(self):

        print('usecurrent =', self.var_applycompositions_usecurrent.get())
        if self.var_applycompositions_usecurrent.get()==True:
            try:
                result = applyindirectcompositions.applyindirectcompositions(
                        relativecompfile=self.const_predcomppath,
                        referencefile=self.const_refnodepath,
                        )
            except:
                self.updateconsole('Error. No files specified. Running normal function instead.')
                result = applyindirectcompositions.applyindirectcompositions()
        else:
            result = applyindirectcompositions.applyindirectcompositions()

        try:
            self.const_abscomppath = result['resultpath']
            self.const_abscompdir = result['resultdir']
            self.const_abscompquantpath = result['quantpath']
            self.toggleapplycompbuttons('normal')
        except:
            pass

    def toggleapplycompbuttons(self, state):
#        state2 = 'disabled' if state=='normal' else 'normal'
#        if (self.openabscomp_button['state']==state2) : self.openabscomp_button['state'] = state
        self.openabscomp_button.state(['!disabled'])
        pass


    def on_openabscomp_button_click(self):
        startfile(self.const_abscomppath)
        pass


    def on_quant_button_click(self):
        graphquant.graphquant(quant_file=self.const_abscompquantpath if self.var_quant_usecurrent.get()==True else '',
                              threshold_percent=self.entry_quant_threshold.get())
        pass

    def scrollbar_moved(self, e=None):
        self.mainwindow.yview(e)


    def _applystyles(self):
        s = ttk.Style()
        s.theme_use(themename='clam')

        palette = dict(contrast='#F76954',
                       text='#000000',
                       dark='#CFC6CB',
                       midtone='#FFE9C8',
                       highlight='#81D9E8',
                       lowlight='#DEFAF5',
                       bg='#F8FEFF')
        

        self.mainwindow.configure(background=palette['bg'])
        
        scalestyle = dict(troughcolor=palette['midtone'],
                          background=palette['bg'],
                          relief='flat')        
        self.scale_plotsub_max.configure(scalestyle)
        self.scale_plotsub_min.configure(scalestyle)
        
        
        s.configure('TLabelframe',              
              background=palette['bg'])
        s.configure('TButton',
                    background=palette['midtone'],
                    foreground=palette['text'])
        s.configure('key.TButton',
                    background=palette['lowlight'],
                    foreground=palette['text'],
                    font=('Arial', '12', 'bold')
                    )
                                        
        



        s.map('TButton',
                foreground=[('pressed', 'black'),
                            ('disabled', 'grey'),
                            ('active', 'blue'),
                            ],
                background=[('pressed', palette['highlight']),
                            ('disabled', 'lightgrey'),
                            ('active', palette['highlight']),
                            ],
                highlightcolor=[('active', 'orange'),
                                ('focus','pink'),
                                ],
                relief=[('pressed', 'groove'),
                        ('!pressed', 'raised'),
                        
                        ])

##        s.map('key.TButton',
##                font=[('weight', 'bold'),                            
##                            ],
##              )


        
        
        s.map('TCheckbutton',
              foreground=[('pressed', 'blue'),                          
                            ],
                background=[('pressed', 'salmon'),
                            ('active', 'lightpink'),
                            ('selected', 'salmon')
                            ],
                relief=[('pressed', 'groove'),
                        ('!pressed', 'ridge'),                        
                        ],                
                        )

        s.map('TEntry',
              foreground=[('hover', 'blue'),
                          ('focus', 'blue'),
                            ],
              background=[('hover', 'blue'),
                          ('focus', 'red'),
                            ],
              )
        
                          
        



     
    
if __name__ == '__main__':
    print('initialising GlycopeptideGraphMS graphical user interface\n\nPlease wait, loading...')
    root = tk.Tk()       
    root.title("GraphMS - by Matthew Choo")
    root.focus_set()
    
    try:
        app = Application(root)
        root.iconbitmap(systemfolder('resources/icon.ico'))
        root.mainloop()
    except ValueError:
        
        root.destroy()
    

    

  
