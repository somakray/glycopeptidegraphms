"""
Uses toy data to show a programmer stepping through line by line how it works

the main function is run_toy_data()
This will execute through each of the steps of the basic functionality of GraphMS.
"""

from graphms import cleanconsensus, graphanalysis, predictcompositions
from graphms import applyindirectcompositions
import shutil, os, glob

# sets the current working directory for stability of filepaths
dirpath = os.path.dirname(os.path.realpath(__file__))
print('setting directory to {}'.format(dirpath))
os.chdir(dirpath)

def run_toy_data():

    cleaned_file = run_cleanconsensus_toy()

    # graphms_result is keyed to result = dict(savefolder=savefolder,
    #                   outgraphpathlist=outgraphpathlist,
    #                   )
    graphms_result = run_graphms_toy(toy_file=cleaned_file)

    # output here is a dict keyed with {predcomp, refnodes}
    predcomp_result = predictcompositions.predictcompositions(graphms_result['outgraphpathlist'],
                                                              predcomp_filepath='../examples/01_graphms_toy_results_folder/predcomp.xlsx')

    # abscomp_result is dict(resultpath=os.path.splitext(savefile)[0]+'.xlsx',
    #                   resultdir=os.path.dirname(savefile),
    #                   quantpath=os.path.splitext(savefile)[0]+'_outPepQuant.xlsx',
    #                   )
    abscomp_result = applyindirectcompositions.applyindirectcompositions(relativecompfile=predcomp_result['predcomp'],
                                                                        referencefile=predcomp_result['refnodes'],
                                                                        savefile='../examples/01_graphms_toy_results_folder/abscomp.xlsx')



    return


def run_cleanconsensus_toy():
    f = '../examples/Example_Data_AXL_truncated.csv'
    cleaned_filelist = cleanconsensus.clean([f])
    cleaned_file = cleaned_filelist[0]
    return cleaned_file


def run_graphms_toy(toy_file):
    """
    :param toy_file: the path to the cleaned consensusxml .csv file
    :return: graphms_result is keyed to result = dict(savefolder=path, outgraphpathlist=[graphmls],

    """
    # clear the directory
    output_folder_path = '../examples/01_graphms_toy_results_folder'
    if os.path.exists(output_folder_path):
        # delete existing folder
        try:
            shutil.rmtree(output_folder_path)
        except PermissionError as e:
            [os.unlink(x) for x in glob.glob('../examples/01_graphms_toy_results_folder/*')]

    # create output folder
    os.mkdir(output_folder_path)

    result = graphanalysis.dographanalysis(inipath='../examples/graphanalysis_default_parameters.ini',
                                          consensuspath=toy_file,
                                          jumplistpath='../examples/JumpList_default_parameters.csv',
                                          outputfolderpath=output_folder_path)

    return result




if __name__ == '__main__':
    run_toy_data()