"""
the aim here is to write the abscomp output
onto the nodes of a graphml file
and then plot it.

"""


from graphms import bokehgraphms, custom_tools, glycanformat, glymod_functions
from graphms.graphanalysis import update_status_threaded
import networkx as nx
import pandas as pd
import numpy as np
import os

INT_TYPE = int


def parse_graph_input_file(flexible_graph_input):

    print('input is ', flexible_graph_input)

    if isinstance(flexible_graph_input, nx.Graph):
        return flexible_graph_input
    elif isinstance(flexible_graph_input, str):
        # if it is a filename to the graphml file
        try:
            return nx.read_graphml(path=flexible_graph_input, node_type=int)
        except:
            raise FileNotFoundError
    elif isinstance(flexible_graph_input, tuple):
        # if a list of filenames of multiple graphml files
        for s in flexible_graph_input:
            try:
                G, outfile = bokehgraphms.graphmls_to_one(flexible_graph_input)
                update_status_threaded('combined graph was exported to: ', outfile)
                return G
            except:
                raise FileNotFoundError

    else:
        raise FileNotFoundError
#
def parse_single_graph_input_file_into_nxgraph(single_graph_or_graphml):
    print('input is ', single_graph_or_graphml)

    if isinstance(single_graph_or_graphml, nx.Graph):
        return single_graph_or_graphml
    elif isinstance(single_graph_or_graphml, str):
        # if it is a filename to the graphml file
        try:
            return nx.read_graphml(path=single_graph_or_graphml, node_type=int)
        except:
            raise FileNotFoundError
    else:
        raise FileNotFoundError


class update_graphtuple_with_abscomp_return_graphtuple:
    """
    take in a tuple of graphs. These have to have been converted to a tuple of Graphs.
    cycle through, annotate with abscomp
    write a new graphml per graph inputted.

    upon initialising, will require the data and graphs
    will then automatically update the graphs
    but not export.
    to export, run: update_graphtuple_with_abscomp_return_graphtuple.export_graphs_to_graphml(optional savedir)




    """
    def __init__(self, abscomp_filepath, graphs_path_tuple, savedir):
        self.savedir = savedir
        self.savefilelist = []

        # access and parse the abscomp into a df
        self.abscomp_filepath = abscomp_filepath
        self.abscomp_df = pd.read_excel(self.abscomp_filepath, index_col="node", header=0)

        # if this was a tuple of graphs or tuple of filepaths, the final result is made to be a tuple anyway
        graphs_path_tuple = graphs_path_tuple if isinstance(graphs_path_tuple, tuple) else (graphs_path_tuple,)

        # this converts the variable into a tuple of nx.Graphs
        self.graphs_tuple = tuple(parse_single_graph_input_file_into_nxgraph(graphitem) for graphitem in graphs_path_tuple)

        # collect the glycan nomenclature from the zeroth node and set them as class properties for later naming
        self.modcol_list, self.modextract_list, self.abbrev_list = get_glycan_abbrevs_from_single_graph(self.graphs_tuple[0])

        # update the graphs in tuple with the abscomp info
        self.graphs_tuple = self.update_graphs(graphs_tuple=self.graphs_tuple)

        return


    def update_graphs(self, graphs_tuple, rt_tolerance_secs=15, mz_tolerance_da=0.01):
        """
        cycle through the graphs
        find a match in abscomp
        if yes, update the values
        output a new graph

        :return:
        """

        # initialise a list of the new processed graphs, to be converted to tuple later
        global_graph_list = []

        for G in graphs_tuple:
            # convert graph nodes to df
            node_df = bokehgraphms.nodes_to_dataframe(G)

            # match the nodes to the abscomp
            result_node_df = match_node_df_to_abscomp(node_df, self.abscomp_df,
                                                      rt_tolerance_seconds=rt_tolerance_secs,
                                                      mz_tolerance_da=mz_tolerance_da,
                                                      abbrev_modextract_list=self.modextract_list)

            # make a dict of the peptide values, keyed to node
            dict_abscomp_gpep = dict(zip(list(result_node_df.index), list(result_node_df['abscomp_gpep'])))

            # make a dict of the protein names, keyed to node
            dict_abscomp_protein = dict(zip(list(result_node_df.index), list(result_node_df['protein'])))

            # make a dict of dicts of the peptide and glycan compositions, keyed to node
            # and for graph G, add the attributes onto the graph.
            nx.set_node_attributes(G, name='abscomp_gpep', values=dict_abscomp_gpep)
            nx.set_node_attributes(G, name='protein', values=dict_abscomp_protein)

            # store the abbreviation key
            abbrev_key_zip = zip(self.modextract_list, self.abbrev_list)

            # label the node according to the SNFG notation
            for gly, abbrev in abbrev_key_zip:
                # create a dictionary with keys=node indexes of result_node_df, values=glycan value for current gly in iteration
                temp_dict_glycans = dict(zip(list(result_node_df.index), np.array(result_node_df[gly], dtype=np.int)))
                # use the jumplist's glycan name's attribute in the node to the dictionary of values keyed by index
                nx.set_node_attributes(G, name=gly, values=temp_dict_glycans)

            # append this new graph to the growing list
            # because you don't want to edit a list as you are iterating through it
            global_graph_list.append(G)

        # force the list into a tuple and return it
        return tuple(global_graph_list)


    def export_graphs_to_graphml(self, savedir=''):
        # if no savedir provided, use the loaded savedir
        savedir = savedir or self.savedir

        # initialise a global list of saved filenames
        savefile_list = []

        # iterate and export graphs
        for index, G in enumerate(self.graphs_tuple):

            # if there has been an ID, use that for the filename instead of the generic filename
            # use get in case the kay doesn't exist, e.g. for graphs not subjected to matching.
            # but make sure you're not checking the zeroth key, which is used for plotting purposes
            id_check = G.node[get_one_nonzero_item_from_list(list(G.nodes()))].get('abscomp_gpep', '')
            if id_check != '':
                path_graphml = os.path.abspath(
                os.path.join(savedir, '_' + str(id_check) + '_' + str(index).zfill(5) + '.graphml'))
            else:
                path_graphml = os.path.abspath(
                    os.path.join(savedir, 'unknown_' + str(index).zfill(5) + '.graphml'))

            nx.write_graphml(G, path=path_graphml)
            savefile_list.append(path_graphml)

        # record the list of save files
        self.savefilelist = savefile_list

        return savefile_list

    def plot_graphs(self):

        p = bokehgraphms.subgraphfigure(has_alignment_data=True)

        p.add_subgraphs_to_plot(graphmls=self.savefilelist, graphoptions = {'scaling':1,'show_edges':True, 'output_format':'canvas'})




def match_node_df_to_abscomp(node_df, abscomp_df, rt_tolerance_seconds=10, mz_tolerance_da=0.01, abbrev_modextract_list=list('NHFS')):
    """
    do this, but in matrix style, or using the match with position index (arg searching):
    for each row in node_df,
    match the node by mz and rt to a value in the abscomp
    write the abscomp parameters to the node_df (more is better rather than less)
    writes the peptide seq, and the glycan composition also as N,H,F,S columns
    Doneon 301118

    NB: this will actually alter the input *node_df* !

    :param node_df:
    :param abscomp_df:
    :return:
    """

    # make sure to add the columns' default values to node_df. otherwise on negative result, no column will be added!
    node_df['abscomp_gpep']=''
    node_df['protein'] = ''



    for gly in abbrev_modextract_list:
        node_df[gly] = 0


    # first make a subtraction matrix of the rt and mz
    # rows = node_df, cols = abscomp_df
    subtraction_matrix_rt = node_df[['rt']].values - abscomp_df[['rt_node']].T.values
    subtraction_matrix_mz = node_df[['mz']].values - abscomp_df[['mz_node']].T.values

    # get True where the values are within the tolerances.
    boolean_mask_rt = (subtraction_matrix_rt > -rt_tolerance_seconds) & (subtraction_matrix_rt < rt_tolerance_seconds)
    boolean_mask_mz = (subtraction_matrix_mz > -mz_tolerance_da) & (subtraction_matrix_mz < mz_tolerance_da)

    # merge the two, to find RT and mz matches
    boolean_mask_rtmz = boolean_mask_mz & boolean_mask_rt


    # then filter out the arg(2D index) of those values that are within the absolute tolerances
    arr_nodedf_match_indices, arr_abscompdf_match_indices = np.where(boolean_mask_rtmz)



    # escape if no matches found and return the original node_df unchanged.
    if len(arr_nodedf_match_indices)==0:
        return node_df

    # otherwise, append the node_df with the matches from abscomp_df
    # the second term has to be a list, because for some reason, a Series is not parsed properly to the column
    node_df.loc[list(node_df.index[arr_nodedf_match_indices]),'abscomp_gpep'] = \
        list(abscomp_df.iloc[list(arr_abscompdf_match_indices)]['pep'])
    try:
        node_df.loc[list(node_df.index[arr_nodedf_match_indices]), 'protein'] = \
            list(abscomp_df.iloc[list(arr_abscompdf_match_indices)]['protein'])
    except:
        # when no protein is specified, ie only the peptide identification
        update_status_threaded('No protein column was found in the ABSOLUTE COMPOSITION dataframe. Skipping protein annotation.')
        pass

    # specify the glycan compositions also, from string
    # by first splitting the compositions into their components
    glycancomp = pd.DataFrame(abscomp_df['composition'].apply(lambda x: glycanformat.glycan_get_comp(x) if isinstance(x,str) else {'N':0,'H':0,'F':0,'S':0}))

    for gly in abbrev_modextract_list:
        try:
            abscomp_df[gly] = glycancomp['composition'].apply(lambda x: x[gly])
        except KeyError as e:
            # print('KeyError - the absolute composition does not match the graphml files. Try rerunning the latest GraphMS version on the consensusxml again.')
            abscomp_df[gly] = 0
        node_df.loc[list(node_df.index[arr_nodedf_match_indices]), gly] = \
            list(abscomp_df.iloc[arr_abscompdf_match_indices][gly])




    # and return the modified node_df and a dictionary keyed to nodes and having the required values
    return node_df


def get_one_nonzero_item_from_list(list_in):
    """
    assumes list is more than zero and one other item.
    :param list_in:
    :return:
    """
    return list_in[-2] if list_in[-1]==0 else list_in[-1]


def get_glycan_abbrevs_from_single_graph(one_graph):
    """
    1. get one graph, assuming that zeroth node will be representative of all the graphs
    2. reconstruct the jumplist from the html table stored in the zeroth node.
    3. extract out the glycan modifications using glymod_functions._extractglymod
    4. convert glycan mod names into SNFG

    :param one_graph:
    :return:
    """

    # 1. get the first graph, assume that zeroth node will be representative of all the graphs
    # 2. reconstruct the jumplist from the html table stored in the zeroth node.
    jumplist_reconstructed_df = pd.read_html(one_graph.nodes[0]['jumplist_all'])[0]

    # 3. extract out the glycan modifications using glymod_functions._extractglymod
    modcol_list, modextract_list = glymod_functions._extractglymod(df=jumplist_reconstructed_df,
                                                                     delimiter='glymod_')

    # 4. convert glycan mod names into SNFG
    abbrev_list = [glycanformat.get_abbrev_from_mono_table_by_glycan(x) for x in modextract_list]

    # 5. print check
    print('modcol_list=\n{}\nmodextract_list=\n{}\nabbrev_list=\n{}\n'.format(modcol_list,modextract_list,abbrev_list))

    return modcol_list, modextract_list, abbrev_list


def run_and_export_graphmls(abscomp_filepath,graphs_tuple,savedir):
    result = update_graphtuple_with_abscomp_return_graphtuple(abscomp_filepath,
                                                         graphs_tuple,
                                                         savedir)

    new_graphml_savefile_list = result.export_graphs_to_graphml()
    result.plot_graphs()
    return new_graphml_savefile_list


def reject_blank_values(input_value):
    """raises an error if blank value or None is passed, otherwise simply returns the non-blank value.
    :param input_value:
    :return:
    """

    if isinstance(input_value, list):
        if input_value == []: raise ValueError('No file provided. Quitting.')
    elif isinstance(input_value, str):
        if input_value == '': raise ValueError('No file provided. Quitting.')
    else:
        if input_value is None: raise ValueError('No file provided. Quitting.')

    return input_value

def run_with_file_prompts():
    abscomp_filepath = reject_blank_values(custom_tools.getfile('select absolute composition file', '.xlsx'))
    graphs_tuple = reject_blank_values(custom_tools.getfiles('select the graphs to label', '.graphml'))
    savedir = reject_blank_values(custom_tools.getfolder('choose the folder to save the new graphs to'))

    run_and_export_graphmls(abscomp_filepath,graphs_tuple,savedir)


def run_with_defined_inputs_and_outputs_no_plotting(abscomp_filepath, graphs_tuple, savedir):
    """

    :param abscomp_filepath: custom_tools.getfile('select absolute composition file', '.xlsx')
    :param graphs_tuple: custom_tools.getfiles('select the graphs to label', '.graphml')
    :param savedir: custom_tools.getfolder('choose the folder to save the new graphs to')
    :return: list of saved file paths of new graphml files that were created.
    """
    # abscomp_filepath = custom_tools.getfile('select absolute composition file', '.xlsx')
    # graphs_tuple = custom_tools.getfiles('select the graphs to label', '.graphml')
    # savedir = custom_tools.getfolder('choose the folder to save the new graphs to')

    result = update_graphtuple_with_abscomp_return_graphtuple(abscomp_filepath,
                                                              graphs_tuple,
                                                              savedir)

    new_graphml_savefile_list = result.export_graphs_to_graphml()

    return new_graphml_savefile_list


# def add_data_attribute_to_existing_node_in_graph(G, node_index, attribute_name, attribute_value):


def execute():
    nodefile = r'O:\Analytics\Matt\058 Roche haptoglobin\J18091712-HCD graph1\6neuacmerged[41, 7, 9].graphml'
    abscompfile = r'O:\Analytics\Matt\058 Roche haptoglobin\J18091712-HCD graph1\align2\abscomp_byonic_matched_refnodes.xlsx'

    result, dict_abscomp_gpep, dict_compositions = match_node_df_to_abscomp(
        bokehgraphms.nodes_to_dataframe(nx.read_graphml(path=nodefile, node_type=str)),
        pd.read_excel(abscompfile))
    print(result)


if __name__ == "__main__":
    run_with_file_prompts()


