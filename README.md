**README (SKIP DOWN TO SEE UPDATED LIST OF ISSUES AND FIXES)**

**LICENSE**
This file is part of GlycopeptideGraphMS

GlycopeptideGraphMS v2.05
Copyright 2018,2019, Matthew SF Choo, Ph.D; the Bioprocessing Technology Institute, the Agency for Science Technology and Research, Singapore.

    GlycopeptideGraphMS ("this program") is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

**CONTACT**

For any questions please contact the author Matthew SF Choo at `matthew_choo@bti.a-star.edu.sg` or `choo.matt@gmail.com`

**IF YOU USE THIS WORK, PLEASE CITE**

Choo, M.S., Wan, C., Rudd, P.M., Nguyen-Khuong, T., 2019. GlycopeptideGraphMS: Improved Glycopeptide Detection and Identification by Exploiting Graph Theoretical Patterns in Mass and Retention Time. Anal. Chem. 91, 7236–7244. https://doi.org/10.1021/acs.analchem.9b00594

**HOW TO USE?**

Note that GlycopeptideGraphMS takes as input a csv file with the first three columns 
having the headers of "rt_cf, mz_cf, intensity_cf" (OpenMS consensusxml headers). 
These contain RT(secs), m/z(neutral mass in Da), intensity. What this means is that you 
must convert your LCMS features into csv with those three headers in columns 1, 2 and 3. One way is the OpenMS/KNIME method that is 
explained in the manual found in the .zip file and for which the KNIME workflow is provided.

You can download the Windows binary in the .zip file and read the User Manual PDF included with the software.

Or, in Python 3.6 clone this repository, install the required packages (pip install -r requirements.txt) and run graphms-gui.py to start the tkinter GUI (recommended to use GUI).

Alternatively, many of the scripts can be run standalone because they will prompt you for the required files. I prefer to run them inside Pycharm.

**KNOWN ISSUES**

* Updating the Peptide and Protein IDs of nodes using `writenodeid.py` is only compatible with graphml files produced on v2.05+ and on JumpList with SNFG abbreviations used. E.g. Please use `glymod_NeuAc` but *not* `glymod_S`  

* The exe does not work on Mac. The Python code should work, however I have not tested. On request I can try to compile a binary for Mac.

* if OpenMS/KNIME does not correctly handle your mzml files, email me if you would like to use other feature finding software such as MaxQuant or Progenesis (DONE) and I can create a conversion script into the GlycopeptideGraphMS input format

* (FIXED) Trying to compile into Win binary using pyinstaller will cause an error (when you try to run the exe) in bokeh due to an issue with jinja2. I've got a workaround to this. If you try to compile GraphMS yourself using pyinstaller (and any script that requires bokeh for plotting) you will find that the exe will throw a runtime error when importing jinja templates. I made a fix here, which you would have to incorporate into the `bokeh/core/templates.py` file in your environment. Read my solution [here](https://github.com/bokeh/bokeh/compare/master...glycoaddict:patch-1?diff=split) and the stackoverflow explanation [here](https://stackoverflow.com/questions/47811533/cant-perform-this-operation-for-unregistered-loader-type/49024538#49024538)

* (FIXED) An error displaying first a "TypeError: an integer is required" "KeyError: 'difference'" can be due to your Jump_list.csv file being delimited (separated) by a character that is not a comma. The script recognises comma separated values. This problem was reported where the .csv file had been transformed into a semicolon-delimited file.

**UPCOMING CHANGES**

* Module to combine GraphMS with identifications (e.g. Byonic, Proteome Discoverer, etc). This has been implemented for Byonic export of PSMs to a csv file. Found in `pipeline.py`.

* Compatibility with .mzid format

**ADDED FEATURES (NEWEST FIRST)**

* (20-Nov-2019; v2.05) JumpList must now use SNFG abbreviations so that the nodes can be labeled correctly. Now handles all known SNFG abbreviations, which you should use in the JumpList.csv file. 

* (20-Aug-2019; v2.03) Any glycan or modification is now allowed by specifying in the JumpList as the column header glymod_XXX where XXX is your modification. E.g. glymod_Ac for acetylation, glymod_Su for sulphation. Works also in "predict compositions" and "apply compositions". If you want the optional final mass calculation on the glycan composition to be accurate, please use SNFG symbols for glycans, and the symbols: Su sulphation, Ac acetylation, Phos phosphate. These symbols may also be edited in the excel table under resources/monosaccharides.xlsx and then converted into the quicker pickle format using `glycanformat.expand_monosaccharide_table()`. Please note that pickling can be different on different systems so let me know if you encounter a file reading error.

* Converting Progenesis' compound measurements to the "correct" csv format for input. Script and GUI found in `fileconversions/convert-progenesis.py` and the Windows compiled .exe is in the zip file in that same folder. 

* GUI window did not fit on some users' screens. Made the GUI scrollable (branch: `scrollablegui`, see dist folder for Win64 compiled exe in zip file).

**TESTED WITH THESE MASS SPETROMETERS**

* tested on, but not limited to mzml converted from these formats:

    * Thermo raw from Orbitrap Fusion Tribrid

**OPERATING SYSTEM REQUIREMENTS**

GlycopeptideGraphMS requires:

* 64-bit processor

* Windows 7, 8, 10 (these have been tested)

* MS Excel or another program for viewing .xlsx files and .csv files

GlycopeptideGraphMS has been tested on:

* Dell Optiplex 7010, 64-bit Intel Core i7-3770, 8GB RAM, 500GB HDD, running Windows 10 with MS Excel 2010.

* Dell Optiplex 7010, 64-bit Intel Core i7-3770, 16GB RAM, 1TB HDD, running Windows 7 with MS Excel 2010.

The binary GlycopeptideGraphMS.exe is not compatible with: 

* 32-bit systems.

* 32-bit Intel Xeon W3670, 4GB RAM, 500GB HDD, running Windows XP.

* Mac OS (with plans to compile for Mac)
