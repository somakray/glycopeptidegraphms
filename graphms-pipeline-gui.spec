# -*- mode: python -*-

block_cipher = None


a = Analysis(['./graphms/pipeline-gui.py'],
             pathex=[r'C:\Users\choom.000\Documents\glycopeptidegraphms_public'],
             binaries=[],
             datas=[('./graphms/resources','resources'),
                    (r'C:\Users\choom.000\test-env\Lib\site-packages\bokeh\core\_templates','_templates'),                                       
                    ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='CrossRefMS v0.01',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True, icon='./graphms/resources/icon.ico',
          )

# this copies the externally needed files into the dist directory.
from shutil import copy

tocopy=[
    './graphms/JumpList_default_parameters.csv',
    './graphms/graphanalysis_default_parameters.ini',
    './graphms/KNIME_OPENMS_GraphMS_Preprocessing_120318.knwf',
    './graphms/GraphMS_Manual 120318.pdf',
    ]
for f in tocopy:
    try:
        copy(f, './dist')
    except:
        pass



